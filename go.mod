module client

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/go-pg/pg/v10 v10.9.0 // indirect
	github.com/gocarina/gocsv v0.0.0-20210326111627-0340a0229e98 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.2.0
	github.com/jackc/pgproto3/v2 v2.0.7 // indirect
	github.com/jackc/pgx/v4 v4.11.0 // indirect
	github.com/jessevdk/go-flags v1.5.0 // indirect
	github.com/klauspost/cpuid/v2 v2.0.6 // indirect
	github.com/minio/sha256-simd v1.0.0 // indirect
	github.com/prometheus/client_golang v1.10.0 // indirect
	github.com/prometheus/common v0.25.0 // indirect
	github.com/prometheus/procfs v0.6.0 // indirect
	github.com/rs/zerolog v1.22.0
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli/v2 v2.2.0
	github.com/vmihailenco/msgpack/v5 v5.3.1 // indirect
	gitlab.com/jaxnet/common/marshaller v0.0.0-20210111115116-dfe56965e53f
	gitlab.com/jaxnet/common/netstack v0.0.0-00010101000000-000000000000
	gitlab.com/jaxnet/core/indexer v0.0.0-20210518041130-22dce9d416f9
	gitlab.com/jaxnet/core/shard.core v1.6.2-0.20210507145058-b27feef6ab2d
	gitlab.com/jaxnet/hub/ead v0.0.0-20210408095956-aafd3accc402
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/net v0.0.0-20210510120150-4163338589ed // indirect
	golang.org/x/sys v0.0.0-20210514084401-e8d321eab015 // indirect
	golang.org/x/text v0.3.6 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

replace gitlab.com/jaxnet/common/netstack => ./dep/netstack

replace gitlab.com/jaxnet/hub/ead => ./dep/ead

replace gitlab.com/jaxnet/core/indexer => ./dep/indexer
