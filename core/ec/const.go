package ec

import "time"

const (
	CrossShardTx_Beacon_MinWindowBlocks = 6
	CrossShardTx_Shards_MinWindowBlocks = 40

	BeaconAvgBlockGenerationTime = 10 * time.Minute
	ShardAvgBlockGenerationTime  = 10 * time.Second
)
