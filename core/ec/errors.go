package ec

import (
	"client/core/logger"
	"errors"
	"os"
)

var (
	ValidationFailed = errors.New("validation error")
)

func InterruptOnError(err error) {
	if err != nil {
		logger.Log.Error().Err(err).Msg("")
		os.Exit(-1)
	}
}
