package settings

import (
	"client/core/ec"
	"client/core/logger"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

var (
	Conf *Settings
)

type CrossShardConfig struct {
	BeaconMinOperationWindowBlocks int `yaml:"beacon-min-operation-windows-blocks"`
	ShardsMinOperationWindowBlocks int `yaml:"shards-min-operation-windows-blocks"`
}

type ShardConfig struct {
	ID                              shards.ID `yaml:"id"`
	AddressesTxsIndexerInterface    string    `yaml:"addresses-txs-indexer-interface"`
	NonSpentUtxosIndexerHostAndPort string    `yaml:"non-spent-utxos-indexer-host-and-port"`
}

type BlockchainConfig struct {
	NetworkName  string           `yaml:"network-name"`
	RPC          RPCConfig        `yaml:"rpc"`
	CrossShardTx CrossShardConfig `yaml:"cross-shard-tx"`
}

type Settings struct {
	Debug        bool             `yaml:"debug"`
	Blockchain   BlockchainConfig `yaml:"blockchain"`
	Network      *chaincfg.Params
	ReceiverMode ReceiverModeConfig         `yaml:"receiver-mode"`
	Beacon       ShardConfig                `yaml:"beacon,omitempty"`
	Shards       []ShardConfig              `yaml:"shards"`
	ShardsMap    map[shards.ID]*ShardConfig `yaml:"-"`
}

func LoadSettings() (err error) {
	Conf = &Settings{}

	data, err := ioutil.ReadFile("conf.yaml")
	if err != nil {
		return
	}

	err = yaml.Unmarshal(data, Conf)
	if err != nil {
		return
	}

	if Conf.Blockchain.NetworkName == "fastnet" {
		Conf.Network = &chaincfg.FTestNetParams
	}

	// Parameters validation
	beaconMinOperationWindowBlocks := Conf.Blockchain.CrossShardTx.BeaconMinOperationWindowBlocks
	if beaconMinOperationWindowBlocks < ec.CrossShardTx_Beacon_MinWindowBlocks {
		logger.Log.Warn().Int(
			"current-value", beaconMinOperationWindowBlocks).Int(
			"min-secure-value", ec.CrossShardTx_Beacon_MinWindowBlocks).Msg(
			"Min operation window for beacon (that is used in cross-shard tx) " +
				"is set to value, that is less than minimum secure one")
	}

	shardsMinOperationWindowBlocks := Conf.Blockchain.CrossShardTx.ShardsMinOperationWindowBlocks
	if shardsMinOperationWindowBlocks < ec.CrossShardTx_Shards_MinWindowBlocks {
		logger.Log.Warn().Int(
			"current-value", shardsMinOperationWindowBlocks).Int(
			"min-secure-value", ec.CrossShardTx_Shards_MinWindowBlocks).Msg(
			"Min operation window for shards (that is used in cross-shard tx) " +
				"is set to value, that is less than minimum secure one")
	}

	Conf.ShardsMap = make(map[shards.ID]*ShardConfig)
	if Conf.Beacon.AddressesTxsIndexerInterface != "" {
		Conf.ShardsMap[0] = &Conf.Beacon
	}
	for i, shardConf := range Conf.Shards {
		Conf.ShardsMap[shardConf.ID] = &Conf.Shards[i]
	}

	return
}
