package keychain

import (
	"errors"
	"fmt"
	"io/ioutil"
)

const filePath = "pkey.key"

func loadPrivateKey() (
	pKeyHex string, err error) {

	pKeyHexBinary, err := ioutil.ReadFile(filePath)
	if err != nil {
		return
	}

	pKeyHex = string(pKeyHexBinary)
	return
}

func parsePrivateKey(pKey string) (pKeyHex string, err error) {
	if len(pKey) != 64 {
		err = errors.New(fmt.Sprint("invalid private key: (", pKeyHex, ")"))
	}
	// todo : add more verification
	pKeyHex = pKey
	return
}
