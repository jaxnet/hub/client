package keychain

import (
	"client/core/settings"
	"encoding/hex"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txmodels"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/types/chainhash"
	"gitlab.com/jaxnet/core/shard.core/types/wire"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
)

// PermanentKeychain handles crypto-related operations that are supposed to be repeated.
// The main idea behind his struct is to isolate the work with sensitive data, like private keys.
type PermanentKeychain struct {
	pKeyHex string
	keyData *txutils.KeyData
}

var (
	KeyChain *PermanentKeychain
)

func InitKeyChain(c *cli.Context, flagPrivateKey string) (err error) {
	var tmpPKeyHex string
	if c.IsSet(flagPrivateKey) {
		tmpPKeyHex, err = parsePrivateKey(c.String(flagPrivateKey))
		if err != nil {
			return
		}
	} else {
		tmpPKeyHex, err = loadPrivateKey()
		if err != nil {
			return
		}
	}
	KeyChain = &PermanentKeychain{
		pKeyHex: tmpPKeyHex,
	}
	KeyChain.keyData, err = txutils.NewKeyData(tmpPKeyHex, settings.Conf.Network)
	if err != nil {
		return
	}
	return
}

func (k *PermanentKeychain) SignCrossShardTX(tx *txmodels.SwapTransaction,
	sourceShard, destinationShard shards.ID, sourceShardLockMultiSig,
	destinationShardLockMultiSig *crypto.MultiSigAddress, txMan *txutils.TxMan) (err error) {

	var swapTxWithMultiSig *wire.MsgTx
	swapTxWithMultiSig, err = txMan.WithKeys(k.keyData).ForShard(uint32(destinationShard)).AddSignatureToSwapTx(tx.RawTX,
		[]uint32{uint32(sourceShard), uint32(destinationShard)},
		sourceShardLockMultiSig.RedeemScript, destinationShardLockMultiSig.RedeemScript)

	if err != nil {
		return
	}

	tx.RawTX = swapTxWithMultiSig
	tx.SignedTx = txutils.EncodeTx(swapTxWithMultiSig)
	tx.TxHash = swapTxWithMultiSig.TxHash().String()
	return
}

func (k *PermanentKeychain) NewTxMan(shardID shards.ID) (txMan *txutils.TxMan, err error) {
	gatlingRPCParams := txutils.ManagerCfg{
		Net: settings.Conf.Blockchain.NetworkName,
		RPC: txutils.NodeRPC{
			Host: settings.Conf.Blockchain.RPC.Net.Interface(),
			User: settings.Conf.Blockchain.RPC.Credentials.User,
			Pass: settings.Conf.Blockchain.RPC.Credentials.Pass,
		},

		// ToDo: [Post PoC] [security]
		// 		 Gatling manager must not store private key in memory.
		PrivateKey: k.pKeyHex,
		ShardID:    uint32(shardID),
	}

	txMan, err = txutils.NewTxMan(gatlingRPCParams)
	if err != nil {
		return
	}

	txMan = txMan.ForShard(uint32(shardID))
	return
}

func (k *PermanentKeychain) PubKey() (pubKey *btcec.PublicKey) {
	pubKey = k.keyData.PrivateKey.PubKey()
	return
}

func (k *PermanentKeychain) AddressPubKey() (addressPubKey *btcutil.AddressPubKey) {
	addressPubKey = k.keyData.AddressPubKey
	return
}

func (k *PermanentKeychain) AddressPubKeyHash() (address *btcutil.AddressPubKeyHash) {
	address = k.keyData.AddressPubKey.AddressPubKeyHash()
	return
}

func (k *PermanentKeychain) AddressPubKeyString() (address string) {
	address = k.keyData.AddressPubKey.String()
	return
}

func (k *PermanentKeychain) KeyDada() *txutils.KeyData {
	return k.keyData
}

func (k *PermanentKeychain) SignDataForIndexer(data string) (pubKey, sig string, err error) {
	hash := chainhash.DoubleHashB([]byte(data))
	signature, err := k.keyData.PrivateKey.Sign(hash)
	if err != nil {
		return
	}

	pubKey = hex.EncodeToString(k.keyData.PrivateKey.PubKey().SerializeUncompressed())
	sig = hex.EncodeToString(signature.Serialize())
	return
}
