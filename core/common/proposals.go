package common

import (
	"gitlab.com/jaxnet/hub/ead/core/epl"
	"math/big"
	"sort"
)

type SortedProposalsList struct {
	Proposals []*epl.ExchangeAgentProposal
}

func NewSortedProposalsList() (list *SortedProposalsList) {
	list = &SortedProposalsList{
		Proposals: make([]*epl.ExchangeAgentProposal, 0),
	}

	return
}

func (sl *SortedProposalsList) MergeProposals(list *epl.ExchangeAgentsProposalsList, requiredAmount *big.Int) {
	newProposals := make([]*epl.ExchangeAgentProposal, 0)

	isProposalUnique := func(proposal *epl.ExchangeAgentProposal) (bool, int) {
		for i, presentProposal := range sl.Proposals {
			// The same EA is already present.
			// Proposals must be compared to determine which one is the newest
			// and should be included into the collected Proposals list.
			if proposal.ExchangeAgentID == presentProposal.ExchangeAgentID {
				return false, i
			}
		}

		return true, -1
	}

	for _, receivedProposal := range list.Proposals {
		isUnique, i := isProposalUnique(receivedProposal)
		if isUnique {
			newProposals = append(newProposals, receivedProposal)

		} else {
			if receivedProposal.SequenceAnchor > sl.Proposals[i].SequenceAnchor {
				sl.Proposals[i] = receivedProposal
			}
		}
	}

	sl.Proposals = append(sl.Proposals, newProposals...)
	sl.sort(requiredAmount)
}

func (sl *SortedProposalsList) sort(dealAmount *big.Int) {
	// PoC limitation: sorting criteria.
	// Various client implementations could have various criteria how to sort Proposals.
	// Current implementation only looks for the cheapest fees,
	// but it is not pretending to be the best option possible.

	sort.Slice(sl.Proposals, func(i, j int) bool {
		proposalI := sl.Proposals[i]
		iFee := proposalI.FinalFee(dealAmount)

		proposalJ := sl.Proposals[j]
		jFee := proposalJ.FinalFee(dealAmount)

		return iFee.Cmp(jFee) == -1
	})
}
