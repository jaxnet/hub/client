package common

import (
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"io/ioutil"
)

func LoadCredentials(filePath string, params *chaincfg.Params) (
	pKey *btcec.PrivateKey, pKeyHex string, pubKey *btcec.PublicKey, address *btcutil.AddressPubKeyHash, err error) {

	pKeyHexBinary, err := ioutil.ReadFile(filePath)
	if err != nil {
		return
	}

	pKeyHex = string(pKeyHexBinary)
	k, err := txutils.NewKeyData(pKeyHex, params)
	if err != nil {
		return
	}

	pKey = k.PrivateKey
	pubKey = pKey.PubKey()
	address = k.AddressPubKey.AddressPubKeyHash()
	return
}
