//
//
// IMPORTANT!
// This file contains validations that must be implemented in real world client.
//
//

package discovery

import (
	"client/core/logger"
	"github.com/davecgh/go-spew/spew"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/discovery"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"gitlab.com/jaxnet/hub/ead/core/epl"
	"time"
)

// filterIrrelevantDiscoveryRecords checks each fetched discovery row if it contains
// both source and destination shard in list of operable shards.
// Client must validate this info each time when it is fetched from the block-chain.
func filterIrrelevantDiscoveryRecords(requiredShards shards.Pair, records []*discovery.DiscoveryRecord) (
	validRecords []*discovery.DiscoveryRecord) {

	validRecords = make([]*discovery.DiscoveryRecord, 0)

	for _, record := range records {
		for _, operatingShards := range record.Addresses {
			for _, shardID := range operatingShards {
				if shardID == requiredShards.SourceShard || shardID == requiredShards.DestinationShard {
					validRecords = append(validRecords, record)
					break
				}
			}
		}
	}

	return
}

func validateFetchedProposalsList(
	list *epl.ExchangeAgentsProposalsList,
	discoveryInfo []*discovery.DiscoveryRecord) (validProposals *epl.ExchangeAgentsProposalsList, err error) {

	logValidationFailed := func(proposal *epl.ExchangeAgentProposal, reason string) {
		logger.Log.Warn().Str(
			"proposal", spew.Sdump(proposal)).Str(
			"reason", reason).Msg(
			"Proposal validation failed")
	}

	validProposals = epl.NewExchangeAgentsProposalsList()
	for _, proposal := range list.Proposals {

		// Looking for corresponding EA into the fetched earlier list of agents.
		var correspondingAgent *discovery.DiscoveryRecord
		for _, agent := range discoveryInfo {
			if agent.SeqNumber == proposal.ExchangeAgentID {
				correspondingAgent = agent
				break
			}
		}

		if correspondingAgent == nil {
			// No corresponding agent has been found.
			// This proposal should not pass validation.
			logValidationFailed(proposal,
				"No corresponding EA has been found. The source of proposal could not be validated.")
			continue
		}

		if proposal.Expire.Before(time.Now().UTC()) {
			// Proposal is expired and should not be used any more.
			logValidationFailed(proposal, "Proposal is expired")
			continue
		}

		// todo: enable me back
		proposalBinary, err := proposal.MarshalAllExceptSignature()
		if err != nil {
			logValidationFailed(proposal, "Proposal could not be marshalled")
			continue
		}

		proposalHash := crypto.CalculateHash(proposalBinary)
		if proposal.Signature.Verify(proposalHash[:], correspondingAgent.PubKey) == false {
			// Proposal doesn't pass the signature validation.
			logValidationFailed(proposal, "Signature validation failed")
			continue
		}

		validProposals.Proposals[correspondingAgent.SeqNumber] = proposal
	}

	return
}
