package discovery

import (
	"errors"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/epl"
	"gitlab.com/jaxnet/hub/ead/core/interfaces/tcp/clients"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
	clients2 "gitlab.com/jaxnet/hub/ead/core/messages/clients_messages"
	"math/big"
	"net"
)

const (
	defaultPort = 3000
)

var (
	ErrInvalidDiscoveryRecord = errors.New("invalid discovery record")
	ErrPartialSocketWrite     = errors.New("partial socket write occurred")
)

func fetchProposalViaAddress(addressDiscoveryRecord string, amount *big.Int, shards shards.Pair) (list *epl.ExchangeAgentsProposalsList, err error) {
	conn, err := parseConnectionInfoAndConnect(addressDiscoveryRecord)
	if err != nil {
		return
	}

	bytesWritten, err := conn.Write([]byte{clients2.DefaultProtocolVersion})
	if err != nil || bytesWritten != clients2.ProtocolVersionFieldSize {
		err = ErrPartialSocketWrite
		return
	}

	err = sendProposalsRequest(conn, amount, shards)
	if err != nil {
		return
	}

	response, err := fetchResponse(conn)
	if err != nil {
		return
	}

	list = epl.NewExchangeAgentsProposalsList()
	for _, proposal := range response.Proposals {
		list.Proposals[proposal.ExchangeAgentID] = proposal
	}

	return
}

func sendProposalsRequest(conn net.Conn, amount *big.Int, shards shards.Pair) (err error) {
	request, err := clients2.NewProposalsRequest(shards, amount)
	if err != nil {
		return
	}

	requestBinary, err := request.MarshalBinary()
	if err != nil {
		return
	}

	encoder := marshalling.NewEncoder()
	err = encoder.PutUint8(clients2.ReqProposals)
	if err != nil {
		return
	}

	err = encoder.PutVariadicDataWithByteHeader(requestBinary)
	if err != nil {
		return
	}

	data := encoder.CollectDataAndReleaseBuffers()
	bytesWritten, err := conn.Write(data)
	if err != nil || bytesWritten != len(data) {
		return
	}

	err = conn.(*net.TCPConn).CloseWrite()
	return
}

func fetchResponse(conn net.Conn) (response *clients2.ProposalsResponse, err error) {
	decoder := marshalling.NewDecoderFromReader(conn)

	messageType, err := decoder.GetUint8()
	if err != nil {
		return
	}

	if messageType != clients2.RespProposals {
		err = clients.ErrProtocolError
		return
	}

	data, err := decoder.GetDataSegmentWith2BytesHeader()
	if err != nil {
		return
	}

	response = &clients2.ProposalsResponse{}
	err = response.UnmarshalBinary(data)
	return
}

func parseConnectionInfoAndConnect(addressDiscoveryRecord string) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", addressDiscoveryRecord)
	return
}
