package discovery

import (
	"client/core/common"
	"client/core/keychain"
	"client/core/logger"
	"client/core/settings"
	"encoding/hex"
	"errors"
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/types/btcjson"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/discovery"
	shards2 "gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/epl"
	"math/big"
)

var (
	ErrUnexpectedShardsConfiguration = errors.New("unexpected shards configuration")
)

// todo: add wide comment here.
func DiscoverBestExchangeOffer(requiredAmount btcutil.Amount, shards shards2.Pair) (
	proposal *epl.ExchangeAgentProposal, eaDiscoveryRecord *discovery.DiscoveryRecord,
	eaPubKeyAddress *btcutil.AddressPubKeyHash, exchangeFee btcutil.Amount, err error) {

	// client must fetch discovery records from the Beacon chain.
	discoveryInfo, err := fetchDiscoveryRecords(shards)
	if err != nil {
		return
	}

	// Client must validate that received info contains only discovery records
	// that are pointing to the EAs, that are working in desired pair of shards.
	discoveryInfo = filterIrrelevantDiscoveryRecords(shards, discoveryInfo)

	// Proposals fetching is expected to be done in several rounds.
	// Sorting proposals allows faster processing and lookup of the best one.
	proposals := common.NewSortedProposalsList()

	pullEAsProposals := func(discoveryRecord *discovery.DiscoveryRecord) (err error) {
		for discoveryAddress, _ := range discoveryRecord.Addresses {

			// todo: replace big.Int with btcutil.Amount on network layer.
			// 		 By default protocol is using big.Int for amount processing,
			//		 but chain itself uses btcutil.Amount which is an alias for int64.
			amount := &big.Int{}
			amount.SetInt64(int64(requiredAmount))

			receivedProposals, err := fetchProposalViaAddress(discoveryAddress, amount, shards)
			if err != nil {
				// Remote EAD response could lead to the error here.
				// In this case only this one particular EA address should be omitted,
				// but the rest must be communicated in regular order.
				continue
			}

			if settings.Conf.Debug {
				if len(receivedProposals.Proposals) == 0 {
					logger.Log.Warn().Str(
						"ead-id", discoveryAddress).Msg(
						"No exchange proposal(s) has been received from the EAD")
				} else {
					logger.Log.Debug().Str(
						"ead-id", discoveryAddress).Int(
						"count", len(receivedProposals.Proposals)).Msg(
						"Exchange proposal(s) has been received from the EAD")
				}
			}

			validProposals, err := validateFetchedProposalsList(receivedProposals, discoveryInfo)
			if err != nil {
				return err
			}

			if len(validProposals.Proposals) != len(receivedProposals.Proposals) {
				// WARN: PoC limitation!
				//
				// Here is the EA that is responding with invalid data.
				// It could be due to some network issue, or due to malformed data packed from the EA.
				// Production implementation potentially should stop communication with this EA.
				continue
			}

			proposals.MergeProposals(validProposals, amount)

			// EAD has responded with list of proposals.
			// There is no need to ask it once more via another IPv4 address
			// or another interface (IPv6 / DNS).
			break
		}

		// WARN: PoC limitation!
		// In case if EAD is not responding on this address - try another IPv4 from the IPv4 pool.
		// then IPv6 address pool and then DNS pool.
		return nil
	}

	// To be able to know which EAs are already asked for the proposals,
	// and which are not - the clone of discovery info structure is used.
	// In real world cases this structure could be simply copied from the original one.
	//
	// Here after each round the discovery information about particular EA IS REMOVED,
	// to not to communicate to the same EAD twice, or more, and to be able to know what EAs are still remaining.
	restDiscoveryRecords, err := fetchDiscoveryRecords(shards)
	if err != nil {
		return
	}

	for {
		if len(restDiscoveryRecords) == 0 {
			// No more EA to be contacted left.
			break

		} else {
			discoveryRecord := restDiscoveryRecords[0]
			err = pullEAsProposals(discoveryRecord)
			if err != nil {
				// In case if this EAD responds with error - just switch to another one.
				restDiscoveryRecords = dropDiscoveryAddress(restDiscoveryRecords, 0)
				err = nil
				continue
			}

			// Dropping current EA so it would not be accessed any more.
			restDiscoveryRecords = dropDiscoveryAddress(restDiscoveryRecords, 0)

			// Dropping all other EAs that has been discovered through the communication to the current one.
			// ToDo: tests needed
			noMoreNeededAgentsPositions := make([]int, 0, 1024)
			for _, proposal := range proposals.Proposals {
				for i, agent := range restDiscoveryRecords {
					if agent.SeqNumber == proposal.ExchangeAgentID {
						noMoreNeededAgentsPositions = append(noMoreNeededAgentsPositions, i)
					}
				}
			}

			for _, pos := range noMoreNeededAgentsPositions {
				restDiscoveryRecords = dropDiscoveryAddress(restDiscoveryRecords, pos)
			}
		}
	}

	// Proposals must be sorted, so the client would be able to choose the best one.
	// But PoC implementation does not requires it to be done.
	if len(proposals.Proposals) == 0 {
		err = errors.New("no proposals has been fetched")
		return
	}

	proposal = proposals.Proposals[0]
	exchangeFee = calculateProposalFeeAmount(requiredAmount, proposal)
	for _, prop := range proposals.Proposals {
		currProposalFee := calculateProposalFeeAmount(requiredAmount, prop)
		if currProposalFee < exchangeFee {
			exchangeFee = currProposalFee
			proposal = prop
		}
	}
	if exchangeFee >= requiredAmount {
		err = errors.New("proposal fee is bigger than tx amount")
		return
	}
	eaDiscoveryRecord = nil
	for _, record := range discoveryInfo {
		if record.SeqNumber == proposal.ExchangeAgentID {
			eaDiscoveryRecord = record
			break
		}
	}
	if eaDiscoveryRecord == nil {
		err = errors.New("Can't find discovery record related to best proposal EAD ID")
		return
	}
	pk := eaDiscoveryRecord.PubKey.SerializeUncompressed()
	addressPubKey, err := btcutil.NewAddressPubKey(pk, settings.Conf.Network)
	if err != nil {
		return
	}
	eaPubKeyAddress = addressPubKey.AddressPubKeyHash()
	return
}

func calculateProposalFeeAmount(requiredAmount btcutil.Amount, proposal *epl.ExchangeAgentProposal) btcutil.Amount {
	precision, _, _ := (&big.Float{}).Parse("100000000", 10)

	feePercent := float64(requiredAmount) * proposal.FeePercents
	feePercentBig := big.NewFloat(feePercent)
	feePercentBig = feePercentBig.Mul(feePercentBig, precision)
	feePercentBigInt := new(big.Int)
	feePercentBig.Int(feePercentBigInt)
	proposalFee := new(big.Int)
	proposalFee = proposalFee.Add(feePercentBigInt, proposal.FeeFixedAmount)
	return btcutil.Amount(proposalFee.Uint64())
}

// fetchDiscoveryRecords emulates fetching registration data from the beacon chain.
// In non PoC implementation this method communicates with beacon chain and fetched discovery info of the EAs,
// that are working with desired pair of shards.
func fetchDiscoveryRecords(shards shards2.Pair) (records []*discovery.DiscoveryRecord, err error) {

	txMan, err := keychain.KeyChain.NewTxMan(0)
	if err != nil {
		return
	}

	shardsList := []uint32{uint32(shards.SourceShard), uint32(shards.DestinationShard)}
	addresses, err := txMan.RPC().ListEADAddresses(shardsList, nil)
	if err != nil {
		return
	}

	logger.Log.Info().Msg(fmt.Sprintf("EAD Addresses: %v", addresses))

	for _, agent := range addresses.Agents {
		rawPK, err := hex.DecodeString(agent.PublicKey)
		if err != nil {
			return nil, err
		}

		pubKeyAddr, err := btcutil.NewAddressPubKey(rawPK, settings.Conf.Network)
		if err != nil {
			return nil, err
		}

		discoveryRecord := discovery.DiscoveryRecord{
			SeqNumber: agent.ID,
			Addresses: convertEADAddressToDiscoveryAddress(agent.IPs),
			PubKey:    pubKeyAddr.PubKey(),
		}
		records = append(records, &discoveryRecord)
	}

	// PoC limitation:
	// Current implementation supports only 2 hard coded shards as a fixture,
	// but the same mechanics would be applied in a real-world application too.
	if shards.SourceShard != 1 && shards.SourceShard != 2 ||
		shards.DestinationShard != 1 && shards.DestinationShard != 2 {

		err = ErrUnexpectedShardsConfiguration
		return
	}

	// IMPORTANT!
	// PubKey MUST NOT be stored in the blockchain as a separated data segment.
	// Instead it should be recovered from the signature of the transaction, that has created this record.
	return
}

func convertEADAddressToDiscoveryAddress(eadAddresses []btcjson.EADAddress) (
	discoveryMapAddresses map[string][]shards2.ID) {
	discoveryMapAddresses = make(map[string][]shards2.ID)
	for _, eadAddress := range eadAddresses {
		discoveryAddressKey := fmt.Sprintf("%s:%d", eadAddress.IP, eadAddress.Port)
		var discoveryAddressShards []shards2.ID
		for _, shardID := range eadAddress.Shards {
			discoveryAddressShards = append(discoveryAddressShards, shards2.ID(shardID))
		}
		discoveryMapAddresses[discoveryAddressKey] = discoveryAddressShards
	}
	return
}

func dropDiscoveryAddress(addresses []*discovery.DiscoveryRecord, dropIndex int) []*discovery.DiscoveryRecord {
	addresses[dropIndex] = addresses[len(addresses)-1]
	addresses = addresses[:len(addresses)-1]
	return addresses
}
