package actions

import (
	"client/core/keychain"
	"client/core/logger"
	"github.com/urfave/cli/v2"
)

func GetAddresses(c *cli.Context) (err error) {
	err = keychain.InitKeyChain(c, FlagPrivateKey)
	if err != nil {
		return
	}

	txMan, err := keychain.KeyChain.NewTxMan(0)
	if err != nil {
		return
	}

	clientPubKeyAddressStr := keychain.KeyChain.AddressPubKeyString()

	addresses, err := txMan.RPC().ListEADAddresses(nil, &clientPubKeyAddressStr)
	if err != nil {
		return
	}

	for key, agent := range addresses.Agents {
		logger.Log.Info().Str("key", key).Msg("")
		for _, address := range agent.IPs {
			logger.Log.Info().Str("ip", address.IP).Uint16(
				"port", address.Port).Time("expTime", address.ExpiresAt).Interface(
				"shards", address.Shards).Msg("")
		}
	}
	return
}
