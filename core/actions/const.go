package actions

const (
	// Commands usages
	UsageSend               = "issues transaction to the network"
	UsageReceiverMode       = "launches client in receiver-mode that allows to receive p2p connection for cross-shards payments"
	UsageRegister           = "registers exchange agent into beacon chain"
	UsageGetAddresses       = "gets all addresses registered in blockchain"
	UsageDeleteRegistration = "deletes all addresses from blockchain"
	UsageRefunding          = "refund exchange operation funds"

	// Flags (arguments) names
	FlagSourceShard              = "source-shard"
	FlagDestinationShard         = "destination-shard"
	FlagAmount                   = "amount"
	FlagAddress                  = "address"
	FlagReceiverResolvingAddress = "receiver-resolving-address"
	FlagReceiverPubKey           = "receiver-pub-key"
	FlagPrivateKey               = "private-key"

	FlagAddressAndShards          = "address-and-shards"
	FlagRefundingFilePath         = "refunding-file-path"
	FlagRefundingTxData           = "refunding-tx-data"
	FlagRefundingLockWindowBlocks = "refunding-lock-window-blocks"

	AmountToLockInRegistrationTx            = 1
	RegularTxRoundDelaySeconds              = 15
	RegistrationTxRoundDelaySeconds         = 15
	RegularTxRequiredConfirmationsCnt       = 5
	RegistrationTxRequiredConfirmationsCnt  = 6
	LockFundsRequiredConfirmationsCnt       = 6
	CrossShardTxRequiredConfirmationsCnt    = 6
	FetchCrossShardTxFromEADRetryDelayInSec = 3

	LockFundsMaxCountAttempts = 3
)
