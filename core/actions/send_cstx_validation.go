package actions

import (
	"client/core/actions/context"
	"client/core/ec"
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/txscript"
	"gitlab.com/jaxnet/core/shard.core/types/btcjson"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"gitlab.com/jaxnet/core/shard.core/types/wire"
)

const (
	// CSTX always moves funds between 2 shards only.s
	ShardsCount = 2

	// CSTX is always going to be spent from multi. sig address (funds lock address),
	// so it always has only one input per shard.
	RequiredInputsCountPerShard = 2
	RequiredTotalInputsCount    = RequiredInputsCountPerShard * ShardsCount

	// CSTX always moves funds to only one destination address in one shard,
	// so it always has only one output per shard.
	RequiredOutputsCountPerShard = 2
	RequiredTotalOutputsCount    = RequiredOutputsCountPerShard * ShardsCount
)

func Validate2SigCSTX(ctx *context.CrossShardTXContext) (err error) {
	// Client checks only EAD's signature presence.
	// No other signatures could be there.
	pubKeysRequired := []*btcec.PublicKey{ctx.EADDiscoveryRecord.PubKey}

	//  todo: Separate source shard amount and destination shard amount according to fees.
	//		  (need help of MI) / https://jaxnetwork.atlassian.net/browse/HUB-141
	return validateCSTx(ctx.CrossShardTx.RawTX, ctx.SenderFundsLockTX, ctx.EADLockTxHash,
		ctx.EADPubKeyAddress.String(), ctx.SenderAddress.String(),
		int64(ctx.Amount), int64(ctx.Amount-ctx.EADExchangeFee), pubKeysRequired,
		ctx.SenderEADMultiSig.RawRedeemScript, ctx.EADMultiSig.RawRedeemScript, ctx.TxMan.NetParams)
}

func Validate3SigCSTX(ctx *context.CrossShard3SigTxContext) (err error) {
	// Client checks only EAD's signature presence.
	// No other signatures could be there.
	pubKeysRequired := []*btcec.PublicKey{ctx.EADDiscoveryRecord.PubKey}

	//  todo: Separate source shard amount and destination shard amount according to fees.
	//		  (need help of MI) / https://jaxnetwork.atlassian.net/browse/HUB-141
	return validateCSTx(ctx.CrossShardTx.RawTX, ctx.SenderFundsLockTX, ctx.EADLockTxHash,
		ctx.EADPubKeyAddress.String(), ctx.ReceiverAddress.String(),
		int64(ctx.Amount), int64(ctx.Amount-ctx.EADExchangeFee), pubKeysRequired,
		ctx.SenderEADMultiSig.RawRedeemScript, ctx.EADMultiSig.RawRedeemScript, ctx.TxMan.NetParams)
}

func ValidateReceiver3SigCSTX(cstx *wire.MsgTx, amount int64, receiverAddress string,
	destinationShardFundsLockingTxHash string,
	pubKeysRequiredToSign []*btcec.PublicKey, sourceShardMultiSigRedeemScript,
	destinationShardMultiSigRedeemScript []byte, net *chaincfg.Params) (err error) {

	err = validateTxStructure(cstx)
	if err != nil {
		return
	}

	const nonInitializedShardIndex = -1

	var destinationShardIndex = nonInitializedShardIndex

	for i, output := range cstx.TxOut {
		decodedOutput, err := decodeOutput(output, net)
		if err != nil {
			return err
		}
		if len(decodedOutput.Addresses) != 1 {
			return validationFailed(
				"destination shard output does not contain exactly one address")
		}

		// Check: destination shard output points to the receiver.
		dOutAddress := decodedOutput.Addresses[0]
		if dOutAddress == receiverAddress {
			destinationShardIndex = i
			break
		}
	}

	if destinationShardIndex == nonInitializedShardIndex {
		return validationFailed(
			"There are no out in CSTX which points to the Receiver: " + receiverAddress)
	}

	if cstx.TxIn[destinationShardIndex].PreviousOutPoint.Hash.String() != destinationShardFundsLockingTxHash {
		msg := "corresponding input in destination shard doesn't match lock TX"
		return validationFailed(msg)
	}

	// Check: destination output uses exactly the same amount of tokens,
	// as it has been declared during exchange initialisation.
	if cstx.TxOut[destinationShardIndex].Value != amount {
		return validationFailed(
			"destination shard output uses invalid amount: required = ", amount,
			", actually used = ", cstx.TxOut[destinationShardIndex].Value)
	}

	err = validateTxSignatures(cstx, pubKeysRequiredToSign, sourceShardMultiSigRedeemScript, destinationShardMultiSigRedeemScript)

	return
}

// todo: move this functionality to the EAD.
func validateCSTx(cstx, senderFundsLockTX *wire.MsgTx, eadFundsLockTxHash string,
	eadPubKeyAddress, receiverPubKeyAddress string, sourceShardAmount, destinationShardAmount int64,
	pubKeysRequiredToSign []*btcec.PublicKey,
	sourceShardMultiSigRedeemScript, destinationShardMultiSigRedeemScript []byte, net *chaincfg.Params) (err error) {

	err = validateTxStructure(cstx)
	if err != nil {
		return
	}

	sourceShardInput, destinationShardInput, sourceShardOutput, destinationShardOutput, err :=
		getInputsAndOutputsOfShards(cstx, senderFundsLockTX, eadFundsLockTxHash)
	if err != nil {
		return
	}

	// Check: inputs and outputs are non nil.
	if sourceShardInput == nil {
		return validationFailed("source shard input could not be nil")
	}
	if sourceShardOutput == nil {
		return validationFailed("source shard output could not be nil")
	}
	if destinationShardInput == nil {
		return validationFailed("destination shard input could not be nil")
	}
	if destinationShardOutput == nil {
		return validationFailed("destination shard output could not be nil")
	}

	// There is no need to check source shard input once again.
	// It has been already matched with source shard funds locking TX.

	decodedSourceShardOutput, err := decodeOutput(sourceShardOutput, net)
	if err != nil {
		return
	}

	decodedDestinationShardOutput, err := decodeOutput(destinationShardOutput, net)
	if err != nil {
		return
	}

	//
	// Source shard checks
	//

	// Check: source output points to only one address.
	if len(decodedSourceShardOutput.Addresses) != 1 {
		return validationFailed(
			"source shard output does not contain exactly one address")
	}

	// Check: source output points to the EAD.
	sOutAddress := decodedSourceShardOutput.Addresses[0]
	if sOutAddress != eadPubKeyAddress {
		return validationFailed(
			"source shard output does not point to the EAD: "+
				"required address is: ", eadPubKeyAddress,
			", output points to: ", sOutAddress)
	}

	// Check: source output uses exactly the same amount of tokens,
	// as it has been declared during exchange initialisation.
	if sourceShardOutput.Value != sourceShardAmount {
		return validationFailed(
			"source shard output uses invalid amount: required = ", sourceShardAmount,
			", actually used = ", sourceShardOutput.Value)
	}

	//
	// Destination shard checks
	//

	// Check: destination output points to only one address.
	if len(decodedDestinationShardOutput.Addresses) != 1 {
		return validationFailed(
			"destination shard output does not contain exactly one address")
	}

	// Check: destination shard output points to the receiver.
	dOutAddress := decodedDestinationShardOutput.Addresses[0]
	if dOutAddress != receiverPubKeyAddress {
		return validationFailed(
			"destination shard output does not point to the EAD: "+
				"required address is: ", receiverPubKeyAddress,
			", output points to: ", dOutAddress)
	}

	// Check: destination output uses exactly the same amount of tokens,
	// as it has been declared during exchange initialisation.
	if destinationShardOutput.Value != destinationShardAmount {
		return validationFailed(
			"destination shard output uses invalid amount: required = ", destinationShardAmount,
			", actually used = ", destinationShardOutput.Value)
	}

	err = validateTxSignatures(cstx, pubKeysRequiredToSign,
		sourceShardMultiSigRedeemScript, destinationShardMultiSigRedeemScript)

	// todo: https://jaxnetwork.atlassian.net/browse/HUB-140
	//		 Potential CVE found.

	return
}

func validateTxStructure(cstx *wire.MsgTx) (err error) {
	// Check: TX must have special type (CSTX).
	if !cstx.SwapTx() {
		return validationFailed("tx is not cross shard")
	}

	// Check: amount of inputs correspond to CSTX configuration.
	if len(cstx.TxIn) != RequiredTotalInputsCount {
		return validationFailed(
			"required amount of inputs in transaction is ", RequiredTotalInputsCount, ", ",
			"but actual is ", len(cstx.TxIn))
	}

	// Check: amount of outputs correspond to CSTX configuration.
	if len(cstx.TxOut) != RequiredTotalOutputsCount {
		return validationFailed(
			"required amount of outputs in transaction is ", RequiredTotalOutputsCount, ", ",
			"but actual is ", len(cstx.TxOut))
	}
	return
}

// CSTX does not specifies what inputs/outputs belongs to which shard.
// The only rule present is the next: the first input and first output belongs to the same shard,
// as well as seconds input and second output belongs to the second shard.
// But there is no any metadata in the CSTX that allows to determine
// what input belongs to which shard in terms of shard ID.
//
// getInputsAndOutputsOfShards tries to find input, that corresponds to the sourceShardFundsLockingTx.
// In case if this input is present - all other outputs and the rest input could be identified and returned.
func getInputsAndOutputsOfShards(cstx, sourceShardFundsLockingTx *wire.MsgTx, destinationShardFundsLockingTxHash string) (
	sourceShardInput, destinationShardInput *wire.TxIn,
	sourceShardOutput, destinationShardOutput *wire.TxOut,
	err error) {

	const (
		nonInitializedShardIndex = -1
	)

	var (
		sourceShardFundsLockingTxHash = sourceShardFundsLockingTx.TxHash()
		sourceShardIndex              = nonInitializedShardIndex
		destinationShardIndex         = nonInitializedShardIndex
	)

	for i, input := range cstx.TxIn {
		if input.PreviousOutPoint.Hash.IsEqual(&sourceShardFundsLockingTxHash) {
			sourceShardIndex = i
			break
		}
	}

	if sourceShardIndex == nonInitializedShardIndex {
		msg := "no corresponding input found that points to the original funds lock TX input (source shard)"
		err = validationFailed(msg)
		return
	}

	// todo: [optimisation] modular arithmetic could be applied here.
	destinationShardIndex = sourceShardIndex + 2
	if destinationShardIndex >= 4 {
		msg := "no corresponding input found that points to the original funds lock TX input (destination shard)"
		err = validationFailed(msg)
		return
	}

	if cstx.TxIn[destinationShardIndex].PreviousOutPoint.Hash.String() != destinationShardFundsLockingTxHash {
		msg := "corresponding input in destination shard doesn't match lock TX"
		err = validationFailed(msg)
		return
	}

	sourceShardInput = cstx.TxIn[sourceShardIndex]
	sourceShardOutput = cstx.TxOut[sourceShardIndex]
	destinationShardInput = cstx.TxIn[destinationShardIndex]
	destinationShardOutput = cstx.TxOut[destinationShardIndex]
	return
}

// decodeOutput decodes shard output into internal data structures for further processing and validation.
func decodeOutput(shardOutput *wire.TxOut, net *chaincfg.Params) (
	decodedShardOutput *btcjson.DecodeScriptResult, err error) {
	decodedShardOutput, err = txutils.DecodeScript(shardOutput.PkScript, net)
	if err != nil {
		return
	}

	return
}

// Check: all CSTX inputs must be signed by the subset of public keys received.
// 		  CSTX could be signed partially (only by some subset of participants of the multi sig.),
//		  but this is totally OK.
func validateTxSignatures(cstx *wire.MsgTx, pubKeysRequiredToSign []*btcec.PublicKey,
	sourceShardMultiSigRedeemScript, destinationShardMultiSigRedeemScript []byte) (err error) {

	// todo : validate 1 and 3 inputs
	for _, pubKey := range pubKeysRequiredToSign {
		err = validateTxInputSignature(cstx, 0, pubKey, sourceShardMultiSigRedeemScript)
		if err != nil {
			return
		}
		err = validateTxInputSignature(cstx, 2, pubKey, destinationShardMultiSigRedeemScript)
		if err != nil {
			return
		}

	}
	return
}

func validateTxInputSignature(cstx *wire.MsgTx, inputIdx int, pubKey *btcec.PublicKey,
	multiSigRedeemScript []byte) (err error) {
	signed, err := txscript.CheckIsSignedByPubKey(cstx, inputIdx, multiSigRedeemScript, pubKey)
	invalidSigMessage := fmt.Sprint(
		"invalid or absent signature occurred on input ", inputIdx,
		", this input is required to be signed by key that is corresponding to this pub. key: ", pubKey)

	if err != nil {
		// There is an error reported.
		// Additional context to the validation error could be passed.
		return validationFailed(invalidSigMessage, " Details: ", err.Error())
	}
	if !signed {
		// No error (and no additional context) is here, but validation still fails.
		return validationFailed(invalidSigMessage)
	}
	return
}

// validationFailed is a shortcut method for reporting validation error.
// Simplifies fmt.Errorf() and fmt.Sprint() combination.
func validationFailed(a ...interface{}) error {
	msg := fmt.Sprint(a)
	return fmt.Errorf(fmt.Sprint(msg, " : %w"), msg, ec.ValidationFailed)
}
