package actions

import (
	"client/core/actions/context"
	"client/core/keychain"
	"client/core/logger"
	"errors"
	"github.com/urfave/cli/v2"
	"net"
	"strconv"
	"strings"
	"time"
)

const (
	IPv4AddrLen      = 4
	IPv6AddrLen      = 11
	IdxIp            = 0
	IdxPortInIPv4    = 1
	IdxPortInIPv6    = 8
	IdxExpTimeInIPv4 = 2
	IdxExpTimeInIPv6 = 9
	IdxShardsInIPv4  = 3
	IdxShardsInIPv6  = 10

	IPv4AddrWithShardsLen    = 3
	IPv4AddrWithoutShardsLen = 2
	IPv6AddrWithShardsLen    = 10
	IPv6AddrWithoutShardsLen = 9
	IdxShardsInIPv4Del       = 2
	IdxShardsInIPv6Del       = 9
)

func parseRegistrationContext(c *cli.Context) (ctx *context.TxContext, err error) {

	ctx = &context.TxContext{
		SenderPubKey:  keychain.KeyChain.PubKey(),
		SenderAddress: keychain.KeyChain.AddressPubKeyHash(),
	}

	return
}

func parseAddressReg(addressStr string) (eadAddress context.EADAddress, err error) {
	addressAndShardsArr := strings.Split(addressStr, ":")
	if len(addressAndShardsArr) == IPv4AddrLen {
		eadAddress, err = parseIPv4EADAddressReg(addressAndShardsArr)
	} else if len(addressAndShardsArr) == IPv6AddrLen {
		eadAddress, err = parseIPv6EADAddressReg(addressAndShardsArr)
	} else {
		err = errors.New("Invalid address value " + addressStr)
	}
	return
}

func parseAddressDel(addressStr string) (eadAddress context.EADAddress, err error) {
	addressAndShardsArr := strings.Split(addressStr, ":")
	if len(addressAndShardsArr) == IPv4AddrWithShardsLen || len(addressAndShardsArr) == IPv4AddrWithoutShardsLen {
		eadAddress, err = parseIPv4EADAddressDel(addressAndShardsArr)
	} else if len(addressAndShardsArr) == IPv6AddrWithShardsLen || len(addressAndShardsArr) == IPv6AddrWithoutShardsLen {
		eadAddress, err = parseIPv6EADAddressDel(addressAndShardsArr)
	} else {
		err = errors.New("Invalid address value " + addressStr)
	}
	return
}

func parseIPv4EADAddressReg(addressAndShardsArr []string) (eadAddress context.EADAddress, err error) {
	ipv4 := net.ParseIP(addressAndShardsArr[IdxIp])
	if ipv4 == nil {
		err = errors.New("Invalid IPv4 value " + addressAndShardsArr[IdxIp])
		return
	}
	logger.Log.Debug().Str("ipv4", ipv4.String()).Msg("")
	port, err := parsePort(addressAndShardsArr[IdxPortInIPv4])
	if err != nil {
		err = errors.New("Invalid port value " + addressAndShardsArr[IdxPortInIPv4])
		return
	}
	logger.Log.Debug().Int64("port", port).Msg("")
	expTime, err := parseExpDate(addressAndShardsArr[IdxExpTimeInIPv4])
	if err != nil {
		err = errors.New("Invalid expTimeInDays value " + addressAndShardsArr[IdxExpTimeInIPv4])
		return
	}
	shardsList, err := parseShardsList(addressAndShardsArr[IdxShardsInIPv4])
	if err != nil {
		err = errors.New("Invalid shards list value " + addressAndShardsArr[IdxShardsInIPv4])
		return
	}

	eadAddress = context.EADAddress{
		ipv4,
		port,
		expTime,
		shardsList}
	return
}

func parseIPv6EADAddressReg(addressAndShardsArr []string) (eadAddress context.EADAddress, err error) {
	ipv6str := strings.Join(addressAndShardsArr[:IdxPortInIPv6], ":")
	ipv6 := net.ParseIP(ipv6str)
	if ipv6 == nil {
		err = errors.New("Invalid IPv6 value " + ipv6str)
		return
	}
	logger.Log.Debug().Str("ipv6", ipv6.String()).Msg("")
	port, err := parsePort(addressAndShardsArr[IdxPortInIPv6])
	if err != nil {
		err = errors.New("Invalid port value " + addressAndShardsArr[IdxPortInIPv6])
		return
	}
	logger.Log.Debug().Int64("port", port).Msg("")
	expTime, err := parseExpDate(addressAndShardsArr[IdxExpTimeInIPv6])
	if err != nil {
		err = errors.New("Invalid expTimeInDays value " + addressAndShardsArr[IdxExpTimeInIPv6])
		return
	}
	shardsList, err := parseShardsList(addressAndShardsArr[IdxShardsInIPv6])
	if err != nil {
		err = errors.New("Invalid shards list value " + addressAndShardsArr[IdxShardsInIPv6])
		return
	}

	eadAddress = context.EADAddress{
		ipv6,
		port,
		expTime,
		shardsList}
	return
}

func parseIPv4EADAddressDel(addressAndShardsArr []string) (eadAddress context.EADAddress, err error) {
	ipv4 := net.ParseIP(addressAndShardsArr[IdxIp])
	if ipv4 == nil {
		err = errors.New("Invalid IPv4 value " + addressAndShardsArr[IdxIp])
		return
	}
	logger.Log.Debug().Str("ipv4", ipv4.String()).Msg("")
	port, err := parsePort(addressAndShardsArr[IdxPortInIPv4])
	if err != nil {
		err = errors.New("Invalid port value " + addressAndShardsArr[IdxPortInIPv4])
		return
	}
	logger.Log.Debug().Int64("port", port).Msg("")
	var shardsList []uint32
	if len(addressAndShardsArr) == IPv4AddrWithShardsLen {
		shardsList, err = parseShardsList(addressAndShardsArr[IdxShardsInIPv4Del])
		if err != nil {
			err = errors.New("Invalid shards list value " + addressAndShardsArr[IdxShardsInIPv4Del])
			return
		}
	}

	eadAddress = context.EADAddress{
		ipv4,
		port,
		0,
		shardsList}
	return
}

func parseIPv6EADAddressDel(addressAndShardsArr []string) (eadAddress context.EADAddress, err error) {
	ipv6str := strings.Join(addressAndShardsArr[:IdxPortInIPv6], ":")
	ipv6 := net.ParseIP(ipv6str)
	if ipv6 == nil {
		err = errors.New("Invalid IPv6 value " + ipv6str)
		return
	}
	logger.Log.Debug().Str("ipv6", ipv6.String()).Msg("")
	port, err := parsePort(addressAndShardsArr[IdxPortInIPv6])
	if err != nil {
		err = errors.New("Invalid port value " + addressAndShardsArr[IdxPortInIPv6])
		return
	}
	logger.Log.Debug().Int64("port", port).Msg("")
	var shardsList []uint32
	if len(addressAndShardsArr) == IPv6AddrWithShardsLen {
		shardsList, err = parseShardsList(addressAndShardsArr[IdxShardsInIPv6Del])
		if err != nil {
			err = errors.New("Invalid shards list value " + addressAndShardsArr[IdxShardsInIPv6Del])
			return
		}
		logger.Log.Debug().Int("shardID", int(shardsList[0])).Msg("")
	}

	eadAddress = context.EADAddress{
		ipv6,
		port,
		0,
		shardsList}
	return
}

func parsePort(portStr string) (port int64, err error) {
	port, err = strconv.ParseInt(portStr, 10, 64)
	return
}

func parseExpDate(expTimeInDays string) (expTimeUnix int64, err error) {
	expTimeInDaysNum, err := strconv.ParseUint(expTimeInDays, 10, 16)
	if err != nil {
		return
	}
	logger.Log.Debug().Uint64("expTimeInDays", expTimeInDaysNum).Msg("")
	expTime := time.Now().Add(time.Duration(time.Hour * 24 * time.Duration(int(expTimeInDaysNum))))
	logger.Log.Debug().Time("expTime", expTime).Msg("")
	expTimeUnix = expTime.Unix()
	return
}

func parseShardsList(shardsList string) (shardsListRes []uint32, err error) {
	shardsListArr := strings.Split(shardsList, ",")
	if len(shardsListArr) == 0 {
		err = errors.New("Empty shards list")
		return
	}
	for _, shardIDStr := range shardsListArr {
		shardID, err1 := strconv.ParseUint(shardIDStr, 10, 32)
		if err1 != nil {
			err = err1
			return
		}
		shardsListRes = append(shardsListRes, uint32(shardID))
	}
	return
}
