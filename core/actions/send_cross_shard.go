package actions

import (
	"client/core/actions/context"
	"client/core/keychain"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
)

func processCrossShardTx(c *cli.Context, ctx *context.TxContext) (err error) {

	//
	// Initializing context of the cross shard operation.
	//

	sourceShard, err := ParseShard(c, FlagSourceShard)
	if err != nil {
		return
	}

	destinationShard, err := ParseShard(c, FlagDestinationShard)
	if err != nil {
		return
	}

	swapCtx := &context.CrossShardTXContext{
		TxContext: *ctx,

		DestShardDestAddress: ctx.DestinationAddress,
		Shards: shards.Pair{
			SourceShard:      sourceShard,
			DestinationShard: destinationShard,
		},
	}

	//
	// Initializing txman
	//
	swapCtx.TxMan, err = keychain.KeyChain.NewTxMan(swapCtx.Shards.SourceShard)
	if err != nil {
		return
	}

	// Determining operation schema. There are 2 possible cases:
	// 1. Sender -> Exchange Agent -> Sender;
	// 2. Sender -> Exchange Agent -> Receiver.
	//
	// In first case `source` and `destination` addresses are the same.

	if ctx.SenderAddress.String() == ctx.DestinationAddress.String() {
		return processSenderEASenderTx(c, swapCtx)

	} else {
		swap3SigCtx := &context.CrossShard3SigTxContext{
			CrossShardTXContext: *swapCtx,
		}
		return processSenderEAReceiverTx(c, swap3SigCtx)
	}
}
