package actions

import (
	"client/core/actions/context"
	"client/core/keychain"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
)

// parseContext returns context with common parameters for payment operation.
func parseContext(c *cli.Context) (ctx *context.TxContext, err error) {
	amount, err := ParseAmount(c)
	if err != nil {
		return
	}

	destinationAddress, err := ParseDestinationAddress(c)
	if err != nil {
		return
	}

	ctx = &context.TxContext{
		Amount:             btcutil.Amount(amount),
		SenderPubKey:       keychain.KeyChain.PubKey(),
		SenderAddress:      keychain.KeyChain.AddressPubKeyHash(),
		DestinationAddress: destinationAddress,
	}

	return
}

func Send(c *cli.Context) (err error) {
	err = keychain.InitKeyChain(c, FlagPrivateKey)
	if err != nil {
		return
	}
	ctx, err := parseContext(c)
	if err != nil {
		return
	}

	if c.IsSet(FlagDestinationShard) && c.Uint64(FlagSourceShard) != c.Uint64(FlagDestinationShard) {
		return processCrossShardTx(c, ctx)

	} else {
		return processRegularTx(c, ctx)
	}
}
