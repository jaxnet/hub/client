package actions

import (
	"client/core/settings"
	"errors"
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"math"
	"strconv"
)

func ParseAmount(c *cli.Context) (amount int64, err error) {
	amount = c.Int64(FlagAmount)
	if amount <= 0 {
		err = errors.New("amount is required and could not be less or equal to zero")
	}

	return
}

func ParseShard(c *cli.Context, flag string) (id shards.ID, err error) {
	value, err := strconv.ParseUint(c.String(flag), 10, 64)
	if err != nil {
		err = errors.New(fmt.Sprint("invalid shard id: (", flag, ")"))
		return
	}
	if value >= math.MaxUint32 {
		err = errors.New(
			fmt.Sprint("invalid shard id: (", flag, ")"))
		return
	}

	id = shards.ID(value)
	return
}

func ParseDestinationAddress(c *cli.Context) (address btcutil.Address, err error) {
	address, err = btcutil.DecodeAddress(c.String(FlagAddress), settings.Conf.Network)
	if err != nil {
		msg := "can't parse destination address specified by the flag `" + FlagAddress + "`"
		err = fmt.Errorf(msg+": %w", err)
		return
	}

	return
}

func ParsePrivateKey(c *cli.Context) (pKeyHex string, err error) {
	pKeyHex = c.String(FlagPrivateKey)
	if len(pKeyHex) != 64 {
		err = errors.New(fmt.Sprint("invalid private key: (", pKeyHex, ")"))
	}
	// todo : add more verification
	return
}
