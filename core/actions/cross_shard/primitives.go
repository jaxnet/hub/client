package cross_shard

import (
	"client/core/ec"
	"client/core/logger"
	"errors"
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/types/btcjson"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"gitlab.com/jaxnet/core/shard.core/types/chainhash"
	"gitlab.com/jaxnet/core/shard.core/types/wire"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"time"
)

func Create2Of2LockMultiSig(sender, ead *btcec.PublicKey, refundDeferringPeriod int32, net *chaincfg.Params) (
	multiSig *crypto.MultiSigAddress, err error) {

	senderPubKeyAddress, err := btcutil.NewAddressPubKey(sender.SerializeUncompressed(), net)
	if err != nil {
		return
	}

	eadPubKeyAddress, err := btcutil.NewAddressPubKey(ead.SerializeUncompressed(), net)
	if err != nil {
		return
	}

	signers := []string{
		senderPubKeyAddress.String(),
		eadPubKeyAddress.String(),
	}

	multiSig, err = crypto.MakeMultiSigLockScript(signers, len(signers), refundDeferringPeriod, net)
	return
}

func Create3Of3LockMultiSig(sender, receiver, ead *btcec.PublicKey, refundDeferringPeriod int32, net *chaincfg.Params) (
	multiSig *crypto.MultiSigAddress, err error) {

	senderPubKeyAddress, err := btcutil.NewAddressPubKey(sender.SerializeUncompressed(), net)
	if err != nil {
		return
	}

	receiverPubKeyAddress, err := btcutil.NewAddressPubKey(receiver.SerializeUncompressed(), net)
	if err != nil {
		return
	}

	eadPubKeyAddress, err := btcutil.NewAddressPubKey(ead.SerializeUncompressed(), net)
	if err != nil {
		return
	}

	signers := []string{
		senderPubKeyAddress.String(),
		receiverPubKeyAddress.String(),
		eadPubKeyAddress.String(),
	}

	multiSig, err = crypto.MakeMultiSigLockScript(signers, len(signers), refundDeferringPeriod, net)
	return
}

// PublishTXToTheShard tries to publish signed (!) "tx" to the destination shard ("shardID").
// In case of error - reports to the log and tries once more up to "maxAttempts".
func PublishTXToTheShard(tx *wire.MsgTx, txMan *txutils.TxMan, shardID shards.ID, maxAttempts int) (err error) {

	for attempt := 1; attempt <= maxAttempts; attempt++ {
		txHash, err := txMan.ForShard(uint32(shardID)).RPC().SendRawTransaction(tx)
		if err != nil {
			logger.Log.Debug().Str(
				"attempt", fmt.Sprint(attempt, "/", maxAttempts)).Str(
				"tx", tx.TxHash().String()).Uint32(
				"shard-id", uint32(shardID)).Msg(
				"Can't publish tx to the shard")

			time.Sleep(ec.ShardAvgBlockGenerationTime)
			continue

		} else {
			logger.Log.Info().Str("tx", txHash.String()).Uint32(
				"shard-id", uint32(shardID)).Msg(
				"Cross-shard Tx published in blockchain")
			return err
		}
	}

	description := fmt.Sprintf("can't publish tx %q to the shard %d", tx.TxHash().String(), shardID)
	err = fmt.Errorf(description+": %w", err)
	return
}

func CheckTxMined(tx *wire.MsgTx, txMan *txutils.TxMan, shardID shards.ID, confirmationsRequired int64, maxAttempts int) (err error) {

	checkTxPresence := func(txHash *chainhash.Hash) (found, mined bool, err error) {
		var out *btcjson.GetTxOutResult
		out, err = txMan.ForShard(uint32(shardID)).RPC().GetTxOut(txHash, 0, true, false)
		if err != nil {
			return
		}

		if out == nil {
			out, err = txMan.ForShard(uint32(shardID)).RPC().GetTxOut(txHash, 2, true, false)
			if err != nil {
				return
			}
		}

		if out == nil {
			logger.Log.Debug().Str(
				"tx-hash", txHash.String()).Msg(
				"Corresponding transaction has been not found (mem pool checked)")
			return
		}

		found = true
		mined = out.Confirmations >= confirmationsRequired
		if !mined {
			logger.Log.Debug().Str(
				"tx-hash", txHash.String()).Str(
				"confirmations", fmt.Sprint(out.Confirmations, "/", confirmationsRequired)).Msg(
				"Corresponding transaction been found, but has insufficient confirmations")

		} else {
			logger.Log.Debug().Str(
				"tx-hash", txHash.String()).Str(
				"confirmations", fmt.Sprint(out.Confirmations, "/", confirmationsRequired)).Msg(
				"Corresponding transaction found and mined (OK)")
		}

		return
	}

	sleepRound := func() {
		if shardID == 0 {
			time.Sleep(ec.BeaconAvgBlockGenerationTime)
			return

		} else {
			time.Sleep(ec.ShardAvgBlockGenerationTime)
			return
		}
	}

	var (
		found = false
		mined = false
	)
	for {
		txHash := tx.TxHash()

		// In case of error, repeating each round several times.
		// In case if there is no error during this round - refresh attempts counter.
		for attempt := 1; attempt <= maxAttempts; attempt++ {
			sleepRound()

			found, mined, err = checkTxPresence(&txHash)
			if err != nil {
				err = fmt.Errorf("cant check if tx mined: %w", err)
				continue
			}

			if !found {
				err = errors.New("can't find tx in chain")
				continue
			}

			if !mined {
				break
			}

			return
		}

		if err != nil {
			return
		}
	}
}
