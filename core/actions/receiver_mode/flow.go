package receiver_mode

import (
	"client/core/actions"
	"client/core/actions/receiver_mode/handler"
	"client/core/keychain"
	"client/core/logger"
	"client/core/settings"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/common/netstack/implementation/messages"
	"gitlab.com/jaxnet/common/netstack/implementation/protocols/p2p/cross_shard_3sig"
	"gitlab.com/jaxnet/common/netstack/stack/communicators"
	m "gitlab.com/jaxnet/common/netstack/stack/messages/common"
	"gitlab.com/jaxnet/common/netstack/stack/proto"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
)

func ReceiverMode(c *cli.Context) (err error) {

	conf := settings.Conf.ReceiverMode
	logger.Info().Str(
		"interface", conf.Net.Interface()).Msg(
		"Starting receiver mode")

	err = keychain.InitKeyChain(c, actions.FlagPrivateKey)
	if err != nil {
		return
	}

	h, err := handler.New()
	if err != nil {
		return
	}

	handleSetTxContext := func(ctx *proto.Context, msg m.Message) (
		nextState *proto.State, outgoingMessage m.Message, err error) {

		logger.Info().Msg("New cross shard payment request occurred")

		// No need to check message type, cause netstack does it already.
		request, _ := msg.(*messages.InitCrossShard3SigPayment)

		// todo: validate data
		_, ok := settings.Conf.ShardsMap[shards.ID(request.DestinationShardID)]
		if !ok {
			logger.Log.Info().Msg("There are no config for addresses transactions index. Transaction aborted")
			outgoingMessage = &messages.CrossShardInit3SigPaymentResponse{
				Code: messages.CodeErrInvalidData,
			}
			return
		}

		tx, err := h.InitTransaction(
			request.SourceShardID, request.DestinationShardID,
			request.SenderPubKey, request.EADPubKey, request.Amount)
		if err != nil {
			return
		}

		nextState = cross_shard_3sig.ReceiverAcceptCrossShardTx
		outgoingMessage = &messages.CrossShardInit3SigPaymentResponse{
			TxUUID: tx.UUID,
			Code:   messages.CodeOK,
		}

		return
	}

	handleAcceptCrossShardTx := func(ctx *proto.Context, msg m.Message) (
		nextState *proto.State, outgoingMessage m.Message, err error) {

		var (
			genericError = &messages.CrossShard3SigTxResponse{
				Code: messages.CodeErrServerFault,
			}
		)

		nextState = proto.FinalState
		request, _ := msg.(*messages.CrossShard3SigTx)
		logger.Log.Info().Str(
			"hash", request.Tx.RawTX.TxHash().String()).Str(
			"signed-tx", request.Tx.SignedTx).Str(
			"source", request.Tx.Source).Uint64(
			"source-shard-multi-sig-lock", request.SourceShardMultiSigLockWindowBlocks).Uint64(
			"destination-shard-multi-sig-lock", request.DestinationShardMultiSigLockWindowBlocks).Msg(
			"Received Cross Shard TX from sender (signed by EAD + Sender)")

		err = h.ProcessCrossShardTX(request.TxUUID, request.Tx,
			request.SourceShardMultiSigLockWindowBlocks, request.DestinationShardMultiSigLockWindowBlocks,
			request.EADLockTxHash, request.EADLockTxOutNum)
		if err != nil {
			// todo: Process error and select proper result code here.
			// 		 Now the generic error is returned here.
			outgoingMessage = genericError
			return
		}

		outgoingMessage = &messages.CrossShard3SigTxResponse{
			Code:     messages.CodeOK,
			SignedTx: request.Tx,
		}
		return
	}

	protocol, err := cross_shard_3sig.SenderReceiverCrossShardPayment()
	if err != nil {
		return
	}

	communicator := communicators.NewTCPCommunicator(protocol, logger.Log.Logger)
	err = communicator.Serve(
		conf.Net.Interface(),
		[]proto.StateHandler{
			handleSetTxContext,
			handleAcceptCrossShardTx,
		},
	)

	return
}
