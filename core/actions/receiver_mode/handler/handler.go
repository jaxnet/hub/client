package handler

import (
	"client/core/actions"
	"client/core/actions/cross_shard"
	"client/core/keychain"
	"client/core/logger"
	"client/core/settings"
	"errors"
	"github.com/google/uuid"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txmodels"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"sync"
)

type TransactionsHandler struct {
	txs        sync.Map
	txManagers sync.Map
}

func New() (handler *TransactionsHandler, err error) {

	handler = &TransactionsHandler{}
	return
}

// todo: drop transactions from memory after TX window ends;
//       Handler must have internal goroutine that clears the txs map from obsolete transactions.
//		 https://jaxnetwork.atlassian.net/browse/HUB-75
func (handler *TransactionsHandler) InitTransaction(
	sourceShardID, destinationShardID uint32, senderPubKey, eadPubKey *btcec.PublicKey, amount btcutil.Amount) (
	tx *ReceiverTx, err error) {

	uid, err := uuid.NewRandom()
	if err != nil {
		return
	}

	tx = &ReceiverTx{
		UUID:               uid,
		SourceShardID:      shards.ID(sourceShardID),
		DestinationShardID: shards.ID(destinationShardID),
		SenderPubKey:       senderPubKey,
		EADPubKey:          eadPubKey,
		Amount:             amount,
	}

	logger.Log.Info().Uint32("source-shard-id", sourceShardID).Uint32(
		"destination-shard-id", destinationShardID).Int64(
		"amount", int64(amount)).Msg(
		"New receiver Tx created")

	handler.txs.Store(uid, tx)
	return
}

func (handler *TransactionsHandler) ProcessCrossShardTX(
	uid uuid.UUID, tx *txmodels.SwapTransaction,
	sourceShardMultiSigLockWindowBlocks, destinationShardMultiSigLockWindowBlocks uint64,
	eadLockTxHash string, eadLockTxOutNum uint64) (err error) {

	ctx, err := handler.getTxContext(uid)
	if err != nil {
		return
	}

	// Restoring redeem script to be able to sign the operation.
	receiverPubKey := keychain.KeyChain.PubKey()
	ctx.DestinationShardMultiSig, err = cross_shard.Create3Of3LockMultiSig(
		ctx.EADPubKey, ctx.SenderPubKey, receiverPubKey, int32(destinationShardMultiSigLockWindowBlocks), settings.Conf.Network)
	if err != nil {
		return
	}
	logger.Debug().Str(
		"address", ctx.DestinationShardMultiSig.Address).Str(
		"redeem-script", ctx.DestinationShardMultiSig.RedeemScript).Msg(
		"Destination shard multi sig")

	ctx.SourceShardMultiSig, err = cross_shard.Create3Of3LockMultiSig(
		ctx.SenderPubKey, receiverPubKey, ctx.EADPubKey, int32(sourceShardMultiSigLockWindowBlocks), settings.Conf.Network)
	if err != nil {
		return
	}
	logger.Debug().Str(
		"address", ctx.SourceShardMultiSig.Address).Str(
		"redeem-script", ctx.SourceShardMultiSig.RedeemScript).Msg(
		"Source shard multi sig")

	destinationShardTXMan, err := handler.getOrInitTxMan(ctx.DestinationShardID)
	if err != nil {
		return
	}

	err = actions.CheckEADLockTx(ctx.DestinationShardID, eadLockTxHash, eadLockTxOutNum,
		ctx.DestinationShardMultiSig.Address, ctx.Amount, destinationShardTXMan)
	if err != nil {
		return
	}

	// CSTX validation
	pubKeysRequiredToSign := []*btcec.PublicKey{ctx.EADPubKey, ctx.SenderPubKey}
	err = actions.ValidateReceiver3SigCSTX(tx.RawTX, int64(ctx.Amount), keychain.KeyChain.AddressPubKeyHash().String(),
		eadLockTxHash, pubKeysRequiredToSign, ctx.SourceShardMultiSig.RawRedeemScript,
		ctx.DestinationShardMultiSig.RawRedeemScript, settings.Conf.Network)
	if err != nil {
		return
	}

	err = keychain.KeyChain.SignCrossShardTX(tx, ctx.SourceShardID, ctx.DestinationShardID,
		ctx.SourceShardMultiSig, ctx.DestinationShardMultiSig, destinationShardTXMan)
	if err != nil {
		return
	}

	err = cross_shard.PublishTXToTheShard(tx.RawTX, destinationShardTXMan, ctx.DestinationShardID, 3)
	if err != nil {
		return
	}

	err = cross_shard.CheckTxMined(
		tx.RawTX, destinationShardTXMan, ctx.DestinationShardID, actions.CrossShardTxRequiredConfirmationsCnt, 3)

	return
}

func (handler *TransactionsHandler) getTxContext(uid uuid.UUID) (tx *ReceiverTx, err error) {
	var (
		value interface{}
		ok    = false
	)

	value, ok = handler.txs.Load(uid)
	if !ok {
		err = errors.New("no transaction with exact UUID is present")
		return
	}

	tx = value.(*ReceiverTx)
	return
}

func (handler *TransactionsHandler) getOrInitTxMan(shardID shards.ID) (txMan *txutils.TxMan, err error) {
	var (
		value interface{}
		ok    = false
	)

	value, ok = handler.txManagers.Load(shardID)
	if !ok {
		// No TxMan is present for this one shard.
		// New one must be created.
		txMan, err = keychain.KeyChain.NewTxMan(shardID)
		if err != nil {
			return
		}

		handler.txManagers.Store(shardID, txMan)
		return
	}

	txMan = value.(*txutils.TxMan)
	return
}
