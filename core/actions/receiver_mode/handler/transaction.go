package handler

import (
	"github.com/google/uuid"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
)

type ReceiverTx struct {
	UUID                     uuid.UUID
	SourceShardID            shards.ID
	DestinationShardID       shards.ID
	DestinationShardMultiSig *crypto.MultiSigAddress
	SenderPubKey             *btcec.PublicKey
	EADPubKey                *btcec.PublicKey
	Amount                   btcutil.Amount
	SourceShardMultiSig      *crypto.MultiSigAddress
}
