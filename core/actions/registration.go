package actions

import (
	"client/core/actions/context"
	"client/core/keychain"
	"client/core/logger"
	"client/core/settings"
	"errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/core/indexer/clients/utxos/biggest_first"
	"gitlab.com/jaxnet/core/shard.core/txscript"
	"time"
)

func Register(c *cli.Context) (err error) {
	err = keychain.InitKeyChain(c, FlagPrivateKey)
	if err != nil {
		return
	}
	ctx, err := parseRegistrationContext(c)
	if err != nil {
		return
	}

	eadRegCtx := &context.EADRegistrationTxContext{
		TxContext: *ctx,
	}

	value := c.StringSlice(FlagAddressAndShards)
	for _, addressAndShards := range value {
		parsedAddress, err := parseAddressReg(addressAndShards)
		if err != nil {
			return err
		}
		eadRegCtx.Addresses = append(eadRegCtx.Addresses, parsedAddress)
	}

	return processEadRegistration(eadRegCtx)
}

func processEadRegistration(ctx *context.EADRegistrationTxContext) (err error) {

	ctx.TxMan, err = keychain.KeyChain.NewTxMan(0)
	if err != nil {
		return
	}

	ownerAddressPubKey := keychain.KeyChain.AddressPubKey()

	var scripts [][]byte
	for _, ipAndPort := range ctx.Addresses {
		for _, shardID := range ipAndPort.Shards {

			scriptAddress, err := txscript.EADAddressScript(
				txscript.EADScriptData{
					ShardID:        shardID,
					IP:             ipAndPort.IP,
					Port:           ipAndPort.Port,
					ExpirationDate: ipAndPort.ExpDate,
					Owner:          ownerAddressPubKey,
				})
			if err != nil {
				return err
			}
			scripts = append(scripts, scriptAddress)
			logger.Log.Debug().Msg("Script created")
		}
	}

	shardConf, ok := settings.Conf.ShardsMap[0]
	if !ok {
		err = errors.New("There are no utxos indexer config for beacon")
		return
	}

	clientIndexer, err := biggest_first.New(shardConf.NonSpentUtxosIndexerHostAndPort,
		false, keychain.KeyChain.SignDataForIndexer)
	if err != nil {
		return
	}

	tx, err := ctx.TxMan.
		NewEADRegistrationTx(AmountToLockInRegistrationTx, clientIndexer, scripts...)
	if err != nil {
		return
	}
	logger.Log.Info().Str("hash", tx.TxHash).Msg("Registration tx created")

	ctx.TxHash, err = ctx.TxMan.RPC().ForBeacon().SendRawTransaction(tx.RawTX)
	if err != nil {
		return
	}
	logger.Log.Info().Str("hash", tx.TxHash).Msg("Registration tx sent to blockchain")

	return handleRegistrationTxConfirmations(ctx)
}

func handleRegistrationTxConfirmations(ctx *context.EADRegistrationTxContext) (err error) {

	for {
		out, err := ctx.TxMan.ForShard(0).RPC().GetTxOut(
			ctx.TxHash, 0, true, false)
		if err != nil {
			logger.Log.Err(err).Str(
				"hash", ctx.TxHash.String()).Msg(
				"Can't check transaction in blockchain")
			return err
		}

		if out == nil {
			logger.Log.Debug().Str(
				"hash", ctx.TxHash.String()).Msg(
				"No TX found in chain. Waiting...")
			time.Sleep(time.Second * RegistrationTxRoundDelaySeconds)
			continue
		}

		if out.Confirmations <= RegistrationTxRequiredConfirmationsCnt {
			logger.Log.Debug().Str("hash", ctx.TxHash.String()).Int64(
				"confirmations", out.Confirmations).Msg(
				"TX found, but not enough confirmations")
			time.Sleep(time.Second * RegistrationTxRoundDelaySeconds)
			continue
		}

		logger.Log.Debug().Msg("TX found and mined with enough confirmations")
		return nil
	}
}
