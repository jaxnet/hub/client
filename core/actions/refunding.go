package actions

import (
	"bufio"
	"bytes"
	"client/core/actions/context"
	"client/core/keychain"
	"client/core/logger"
	"encoding/hex"
	"errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/core/shard.core/types/wire"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"os"
	"strconv"
	"time"
)

func Refund(c *cli.Context) (err error) {
	err = keychain.InitKeyChain(c, FlagPrivateKey)
	if err != nil {
		return
	}
	ctx, err := parseRefundingContext(c)
	if err != nil {
		return
	}

	logger.Log.Info().Uint32("shard ID", uint32(ctx.ShardID)).
		Uint64("Lock window blocks", ctx.RefundingLokWindow).
		Msg("Refunding started")

	ctx.TxMan, err = keychain.KeyChain.NewTxMan(ctx.ShardID)
	if err != nil {
		return
	}

	_, currentBlockNumber, err := ctx.TxMan.ForShard(
		uint32(ctx.ShardID)).RPC().GetBestBlock()
	if err != nil {
		logger.Log.Err(err).Msg(
			"Can't get block number from blockchain")
		return
	}

	if currentBlockNumber < int32(ctx.RefundingLokWindow) {
		err = errors.New("Refunding lock window doesn't elapsed yet")
		return
	}

	rawTX := &wire.MsgTx{}
	buf := bytes.NewReader(ctx.RefundingTx)
	err = rawTX.Deserialize(buf)
	if err != nil {
		return
	}
	logger.Log.Info().Str("Hash", rawTX.TxHash().String()).Msg("Restored")

	ctx.TxHash, err = ctx.TxMan.ForShard(uint32(ctx.ShardID)).RPC().SendRawTransaction(rawTX)
	if err != nil {
		return
	}
	logger.Log.Info().Str(
		"hash", ctx.TxHash.String()).Msg(
		"Funds locking tx sent to blockchain")

	return handleRefundingTxConfirmations(ctx)
}

func handleRefundingTxConfirmations(ctx *context.RefundingTxContext) (err error) {

	for {
		out, err := ctx.TxMan.ForShard(uint32(ctx.ShardID)).RPC().GetTxOut(
			ctx.TxHash, 0, true, false)
		if err != nil {
			logger.Log.Err(err).Str(
				"hash", ctx.TxHash.String()).Msg(
				"Can't check transaction in blockchain")
			return err
		}

		if out == nil {
			logger.Log.Info().Str(
				"hash", ctx.TxHash.String()).Msg(
				"No TX found in chain. Waiting...")
			time.Sleep(time.Second * RegistrationTxRoundDelaySeconds)
			continue
		}

		if out.Confirmations <= RegistrationTxRequiredConfirmationsCnt {
			logger.Log.Info().Str("hash", ctx.TxHash.String()).Int64(
				"confirmations", out.Confirmations).Msg(
				"TX found, but not enough confirmations")
			time.Sleep(time.Second * RegistrationTxRoundDelaySeconds)
			continue
		}

		logger.Log.Info().Msg("TX found and mined with enough confirmations")
		return nil
	}
}

func parseRefundingContext(c *cli.Context) (ctx *context.RefundingTxContext, err error) {
	ctx = &context.RefundingTxContext{
		TxContext: context.TxContext{
			SenderPubKey:  keychain.KeyChain.PubKey(),
			SenderAddress: keychain.KeyChain.AddressPubKeyHash(),
		},
	}

	if c.IsSet(FlagRefundingFilePath) {
		_, err = os.Stat(c.String(FlagRefundingFilePath))
		if err != nil {
			return
		} else {
			err = readRefundingDataFromFile(c.String(FlagRefundingFilePath), ctx)
			if err != nil {
				return
			}
		}
	} else if c.IsSet(FlagRefundingTxData) && c.IsSet(FlagRefundingLockWindowBlocks) {
		ctx.RefundingTx, err = hex.DecodeString(c.String(FlagRefundingTxData))
		if err != nil {
			return
		}
		ctx.ShardID, err = ParseShard(c, FlagSourceShard)
		ctx.RefundingLokWindow = c.Uint64(FlagRefundingLockWindowBlocks)
	} else {
		err = errors.New("One of refunding-file-path or refunding-tx-data should be set")
	}
	return
}

func readRefundingDataFromFile(filePath string, ctx *context.RefundingTxContext) (err error) {
	file, err := os.Open(filePath)
	if err != nil {
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	if !scanner.Scan() {
		err = errors.New("Invalid data in refunding file")
		return
	}
	// todo : validate txHash
	ctx.RefundingTx, err = hex.DecodeString(scanner.Text())
	if err != nil {
		return
	}

	if !scanner.Scan() {
		err = errors.New("Invalid data in refunding file")
		return
	}
	shardID, err := strconv.ParseUint(scanner.Text(), 10, 64)
	if err != nil {
		return
	}
	ctx.ShardID = shards.ID(shardID)

	if !scanner.Scan() {
		err = errors.New("Invalid data in refunding file")
		return
	}
	ctx.RefundingLokWindow, err = strconv.ParseUint(scanner.Text(), 10, 64)

	return
}
