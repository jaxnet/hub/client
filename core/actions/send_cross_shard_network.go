package actions

import (
	"bufio"
	marshalling "gitlab.com/jaxnet/common/marshaller"
	"gitlab.com/jaxnet/hub/ead/core/interfaces/tcp"
	"gitlab.com/jaxnet/hub/ead/core/messages"
	"gitlab.com/jaxnet/hub/ead/core/messages/clients_messages"
	"net"
)

func sendRequest(conn net.Conn, req messages.Request) (err error) {

	///Testing:Injection:SendRequestErr
	encoder := marshalling.NewEncoder()
	err = encoder.MarshallVariadicDataWith2BytesHeader(req)
	if err != nil {
		return
	}

	_, err = conn.Write(req.TypeID())
	if err != nil {
		return
	}

	data := encoder.CollectDataAndReleaseBuffers()
	_, err = conn.Write(data)
	return
}

// todo: [validation] Check that signature of the response belongs to the EAD.
func ReceiveResponse(conn net.Conn) (resp tcp.Response, err error) {
	var responseTypeBinary = []byte{0}
	_, err = conn.Read(responseTypeBinary)
	if err != nil {
		return
	}

	responseType := responseTypeBinary[0]
	///Testing:Injection:ReceiveResponseErr
	switch responseType {
	case clients_messages.RespInitExchange:
		resp = &clients_messages.ResponseInitExchange{}

	case clients_messages.RespClientFundsLockTx:
		resp = &clients_messages.ResponseClientFundsLockTx{}

	case clients_messages.RespFetchCrossShardTx:
		resp = &clients_messages.ResponseFetchCrossShardTx{}

	//case clients_messages.RespEADFundsLockingTX:
	//	resp = &clients_messages.ResponseEADFundsLockTx{}
	//
	//case clients_messages.RespCrossShardTx:
	//	resp = &clients_messages.ResponseCrossShardTx{}

	default:
		panic("Unexpected response type")
	}

	reader := bufio.NewReader(conn)
	decoder := marshalling.NewDecoderFromReader(reader)
	err = decoder.UnmarshalDataSegmentWith2BytesHeader(resp)
	return
}
