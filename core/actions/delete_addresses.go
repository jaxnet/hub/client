package actions

import (
	"client/core/actions/context"
	"client/core/keychain"
	"client/core/logger"
	"client/core/settings"
	errors "errors"
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/core/indexer/clients/utxos/biggest_first"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/txscript"
	"gitlab.com/jaxnet/core/shard.core/types/btcjson"
	"net"
	"time"
)

func DeleteAddresses(c *cli.Context) (err error) {
	err = keychain.InitKeyChain(c, FlagPrivateKey)
	if err != nil {
		return
	}
	ctx, err := parseRegistrationContext(c)
	if err != nil {
		return
	}

	eadAddrDelCtx := &context.EADRegistrationTxContext{
		TxContext: *ctx,
	}

	value := c.StringSlice(FlagAddressAndShards)
	for _, addressAndShards := range value {
		parsedAddress, err := parseAddressDel(addressAndShards)
		if err != nil {
			return err
		}
		eadAddrDelCtx.Addresses = append(eadAddrDelCtx.Addresses, parsedAddress)
	}
	return processEADAddressDeletion(eadAddrDelCtx)
}

func processEADAddressDeletion(ctx *context.EADRegistrationTxContext) (err error) {

	ctx.TxMan, err = keychain.KeyChain.NewTxMan(0)
	if err != nil {
		return
	}

	clientPubKeyAddressStr := keychain.KeyChain.AddressPubKeyString()
	addresses, err := ctx.TxMan.RPC().ListEADAddresses(nil, &clientPubKeyAddressStr)
	if err != nil {
		return
	}

	var scripts [][]byte
	for _, address := range ctx.Addresses {
		ok, registeredAddress := findEADAddress(address, addresses)
		if !ok {
			return errors.New(fmt.Sprintf("There are no address %s:%d in blockchain",
				address.IP.String(), address.Port))
		}
		if len(address.Shards) == 0 {
			err = addScriptsForOneAddress(&scripts, address, keychain.KeyChain.AddressPubKey(),
				registeredAddress.Shards, registeredAddress.ExpiresAt)
			if err != nil {
				return
			}
		} else {
			err = addScriptsForOneAddress(&scripts, address, keychain.KeyChain.AddressPubKey(),
				address.Shards, registeredAddress.ExpiresAt)
		}
	}

	shardConf, ok := settings.Conf.ShardsMap[0]
	if !ok {
		err = errors.New("There are no utxos indexer config for beacon")
		return
	}

	clientIndexer, err := biggest_first.New(shardConf.NonSpentUtxosIndexerHostAndPort,
		false, keychain.KeyChain.SignDataForIndexer)
	if err != nil {
		return
	}

	tx, err := ctx.TxMan.
		NewEADRegistrationTx(AmountToLockInRegistrationTx, clientIndexer, scripts...)
	if err != nil {
		return
	}
	logger.Log.Info().Str("hash", tx.TxHash).Msg("Registration tx created")

	ctx.TxHash, err = ctx.TxMan.RPC().ForBeacon().SendRawTransaction(tx.RawTX)
	if err != nil {
		return
	}
	logger.Log.Info().Str("hash", tx.TxHash).Msg("Registration tx sent to blockchain")

	return handleRegistrationDelTxConfirmations(ctx)
}

func handleRegistrationDelTxConfirmations(ctx *context.EADRegistrationTxContext) (err error) {

	for {
		out, err := ctx.TxMan.ForShard(0).RPC().GetTxOut(
			ctx.TxHash, 0, true, false)
		if err != nil {
			logger.Log.Err(err).Str(
				"hash", ctx.TxHash.String()).Msg(
				"Can't check transaction in blockchain")
			return err
		}

		if out == nil {
			logger.Log.Debug().Str(
				"hash", ctx.TxHash.String()).Msg(
				"No TX found in chain. Waiting...")
			time.Sleep(time.Second * RegistrationTxRoundDelaySeconds)
			continue
		}

		if out.Confirmations <= RegistrationTxRequiredConfirmationsCnt {
			logger.Log.Debug().Str("hash", ctx.TxHash.String()).Int64(
				"confirmations", out.Confirmations).Msg(
				"TX found, but not enough confirmations")
			time.Sleep(time.Second * RegistrationTxRoundDelaySeconds)
			continue
		}

		logger.Log.Debug().Msg("TX found and mined with enough confirmations")
		return nil
	}
}

func addScriptsForOneAddress(scripts *[][]byte, eadAddress context.EADAddress, clientPubKey *btcutil.AddressPubKey,
	shards []uint32, registeredExpDate time.Time) error {
	for _, shardID := range shards {
		scriptAddress, err := txscript.EADAddressScript(
			txscript.EADScriptData{
				ShardID:        shardID,
				IP:             net.ParseIP(eadAddress.IP.String()),
				Port:           eadAddress.Port,
				ExpirationDate: registeredExpDate.Unix(),
				Owner:          clientPubKey,
				OpCode:         txscript.EADAddressDelete,
			})
		if err != nil {
			return err
		}
		*scripts = append(*scripts, scriptAddress)
		logger.Log.Debug().Int("shard-id", int(shardID)).Msg("Script created")
	}
	return nil
}

// Try to find address findAddress in registeredAddresses from blockchain
func findEADAddress(findAddress context.EADAddress, registeredAddresses *btcjson.ListEADAddresses) (
	ok bool, eadResAddress *btcjson.EADAddress) {
	ok = false
	for _, registerAddress := range registeredAddresses.Agents {
		for _, eadAddress := range registerAddress.IPs {
			if eadAddress.IP == findAddress.IP.String() && int64(eadAddress.Port) == findAddress.Port {
				ok = true
				eadResAddress = &eadAddress
				return
			}
		}
	}
	return
}
