package actions

import (
	"bufio"
	"client/core/actions/context"
	"client/core/actions/cross_shard"
	"client/core/keychain"
	"client/core/logger"
	"client/core/settings"
	"errors"
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/core/indexer/clients/utxos/biggest_first"
	"os"
)

// todo: [post PoC] [tests] Emulate various connections lost during transaction processing.
func processSenderEASenderTx(c *cli.Context, ctx *context.CrossShardTXContext) (err error) {
	logger.Log.Info().Int64(
		"amount", int64(ctx.Amount)).Uint32(
		"source-shard-id", uint32(ctx.Shards.SourceShard)).Uint32(
		"destination-shard-id", uint32(ctx.Shards.DestinationShard)).Msg(
		"Cross-chain transaction started using S->EA->S schema (2 sig)")

	_, ok := settings.Conf.ShardsMap[ctx.Shards.DestinationShard]
	if !ok {
		msg := fmt.Sprintf("There are no config for addresses transactions index. Transaction aborted")
		logger.Log.Info().Msg(msg)
		fmt.Println(msg)
		return
	}

	shardConf, ok := settings.Conf.ShardsMap[ctx.Shards.SourceShard]
	if !ok {
		err = errors.New("There are no utxos indexer config for shard")
		return
	}

	clientIndexer, err := biggest_first.New(shardConf.NonSpentUtxosIndexerHostAndPort,
		false, keychain.KeyChain.SignDataForIndexer)
	if err != nil {
		return
	}

	//
	// Stage 1: Discovery.
	// Client attempts to discover exchange agents, that are present in the network,
	// fetch their exchange proposals and select the best one.
	logger.Log.Info().Msg("Looking for exchange proposals available...")

	err = discoverAvailableExchangeProposals(ctx)
	if err != nil {
		return
	}

	logger.Log.Info().Uint64(
		"ead", ctx.Proposal.ExchangeAgentID).Int64(
		"fee-fixed", ctx.Proposal.FeeFixedAmount.Int64()).Float64(
		"fee-percents", ctx.Proposal.FeePercents).Msg(
		"Exchange proposal found")

	scanner := bufio.NewScanner(os.Stdin)

	confirm, err := confirmExchangeFee(ctx, scanner)
	if err != nil {
		return
	}
	if !confirm {
		return
	}

	confirm, ctx.SenderFundsLockFee, ctx.Amount, err = confirmNetworkFee(
		ctx.TxMan.ForShard(uint32(ctx.Shards.SourceShard)), ctx.Shards.SourceShard,
		ctx.SenderAddress.EncodeAddress(), ctx.Amount, scanner, clientIndexer)
	if err != nil {
		return
	}
	if !confirm {
		return
	}

	err = confirmRefundTxPath(ctx, scanner)
	if err != nil {
		return
	}

	//
	// Stage 2: Cross-shards operation (swap).
	//

	conn, err := ConnectToEAD(ctx)
	if err != nil {
		return
	}

	err = Init2SigExchangeOperation(ctx, conn)
	if err != nil {
		return
	}

	err = Prepare2Of2MultiSig(ctx)
	if err != nil {
		return
	}

	//
	// Stage 2.1: Multi. sig senderAddress initialisation
	//
	err = LockFundsInMultiSigAddressAndInformEADAboutLock(ctx, conn, clientIndexer)
	if err != nil {
		return
	}

	err = FetchCrossShardTxFromEAD(ctx)
	if err != nil {
		return
	}

	logger.Log.Info().Str(
		"hash", ctx.CrossShardTx.RawTX.TxHash().String()).Str(
		"signed-tx", ctx.CrossShardTx.SignedTx).Str(
		"source", ctx.CrossShardTx.Source).Str(
		"ead-lock-tx-hash", ctx.EADLockTxHash).Uint64(
		"ead-lock-tx-out-num", ctx.EADLockTxOutNum).Msg(
		"Cross-Shard TX received from EAD")

	err = CheckEADLockTx(ctx.Shards.DestinationShard, ctx.EADLockTxHash, ctx.EADLockTxOutNum,
		ctx.EADMultiSig.Address, ctx.Amount-ctx.EADExchangeFee, ctx.TxMan)
	if err != nil {
		return
	}

	err = Validate2SigCSTX(ctx)
	if err != nil {
		return
	}

	err = SignCrossShardTX(ctx)
	if err != nil {
		return
	}

	err = cross_shard.PublishTXToTheShard(ctx.CrossShardTx.RawTX, ctx.TxMan, ctx.Shards.DestinationShard, 3)
	if err != nil {
		return
	}

	err = cross_shard.CheckTxMined(
		ctx.CrossShardTx.RawTX, ctx.TxMan, ctx.Shards.DestinationShard, CrossShardTxRequiredConfirmationsCnt, 3)

	// todo: [post PoC] Sleep some time for transaction to be executed on chain.
	//		 There is no any reason to poll EA until transaction would not be executed on chain,
	//		 so it would be nice to have a time sleep here for average TX execution time.

	// todo: [post PoC] This operation could take fow a while,
	// 		 so current TCP connection could be closed, and new one should be used for further communications.

	return
}
