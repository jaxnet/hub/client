package actions

import (
	"bufio"
	"client/core/actions/context"
	"client/core/keychain"
	"client/core/logger"
	"client/core/settings"
	"errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/core/indexer/clients/utxos/biggest_first"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"os"
	"time"
)

func processRegularTx(c *cli.Context, ctx1 *context.TxContext) (err error) {
	sourceShard, err := ParseShard(c, FlagSourceShard)
	if err != nil {
		return
	}

	sendCtx := &context.RegularTXContext{
		TxContext:     *ctx1,
		SourceShardID: sourceShard,
	}

	//
	// Initializing txman
	//
	sendCtx.TxMan, err = keychain.KeyChain.NewTxMan(sendCtx.SourceShardID)
	if err != nil {
		return
	}

	shardConf, ok := settings.Conf.ShardsMap[sendCtx.SourceShardID]
	if !ok {
		err = errors.New("There are no utxos indexer config for shard")
		return
	}

	clientIndexer, err := biggest_first.New(shardConf.NonSpentUtxosIndexerHostAndPort,
		false, keychain.KeyChain.SignDataForIndexer)
	if err != nil {
		return
	}

	scanner := bufio.NewScanner(os.Stdin)
	var confirm bool

	confirm, sendCtx.TxNetworkFee, sendCtx.Amount, err = confirmNetworkFee(sendCtx.TxMan, sendCtx.SourceShardID,
		sendCtx.SenderAddress.EncodeAddress(), sendCtx.Amount, scanner, clientIndexer)
	if err != nil {
		return
	}
	if !confirm {
		return
	}

	txBuilder := txutils.NewTxBuilder(chaincfg.NetName(settings.Conf.Blockchain.NetworkName)).
		SetSenders(sendCtx.SenderAddress.EncodeAddress()).
		SetShardID(uint32(sendCtx.SourceShardID)).
		SetUTXOProvider(clientIndexer).
		SetDestination(sendCtx.DestinationAddress.EncodeAddress(), int64(sendCtx.Amount)).
		SetChangeDestination(sendCtx.SenderAddress.EncodeAddress())
	tx, err := txBuilder.IntoTx(func(intShardID uint32) (int64, int64, error) {
		return int64(sendCtx.TxNetworkFee), 0, nil
	}, keychain.KeyChain.KeyDada())
	if err != nil {
		return
	}

	logger.Log.Info().Msg("Preparing the send funds transaction")

	txHash, err := sendCtx.TxMan.RPC().SendRawTransaction(tx)
	if err != nil {
		return
	}
	logger.Log.Debug().Str(
		"hash", txHash.String()).Msg(
		"Funds sending tx sent to blockchain")
	sendCtx.TxHash = txHash

	return handleRegularTxConfirmations(sendCtx)
}

func handleRegularTxConfirmations(ctx *context.RegularTXContext) (err error) {

	for {
		out, err := ctx.TxMan.RPC().GetTxOut(
			ctx.TxHash, 0, true, false)
		if err != nil {
			logger.Log.Err(err).Str(
				"hash", ctx.TxHash.String()).Msg(
				"Can't check transaction in blockchain")
			return err
		}

		if out == nil {
			logger.Log.Debug().Str(
				"hash", ctx.TxHash.String()).Msg(
				"No TX found in chain. Waiting...")
			time.Sleep(time.Second * RegularTxRoundDelaySeconds)
			continue
		}

		if out.Confirmations <= RegularTxRequiredConfirmationsCnt {
			logger.Log.Debug().Str("hash", ctx.TxHash.String()).Int64(
				"confirmations", out.Confirmations).Msg(
				"TX found, but not enough confirmations")
			time.Sleep(time.Second * RegularTxRoundDelaySeconds)
			continue
		}

		logger.Log.Debug().Msg("TX found and mined with enough confirmations")
		return nil
	}
}
