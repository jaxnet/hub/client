package context

import (
	"github.com/google/uuid"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txmodels"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/types/chainhash"
	"gitlab.com/jaxnet/core/shard.core/types/wire"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/discovery"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"gitlab.com/jaxnet/hub/ead/core/epl"
	msg "gitlab.com/jaxnet/hub/ead/core/messages/clients_messages"
	"net"
)

type TxContext struct {
	Amount             btcutil.Amount
	SenderPubKey       *btcec.PublicKey
	SenderAddress      *btcutil.AddressPubKeyHash
	DestinationAddress btcutil.Address
}

type RegularTXContext struct {
	TxContext
	SourceShardID shards.ID

	TxHash       *chainhash.Hash
	TxNetworkFee btcutil.Amount

	// TxMan config
	TxMan *txutils.TxMan
}

// todo: [refactor] Rename to CrossShard2SigTxContext
type CrossShardTXContext struct {
	// TX related parameters
	TxContext
	TxUUID                 uuid.UUID
	Shards                 shards.Pair
	SourceShardDestAddress btcutil.Address
	DestShardDestAddress   btcutil.Address

	// Client related parameters
	SenderEADMultiSig  *crypto.MultiSigAddress
	SenderFundsLockTX  *wire.MsgTx
	SenderFundsLockFee btcutil.Amount

	// EA related parameters
	Proposal           *epl.ExchangeAgentProposal
	EADDiscoveryRecord *discovery.DiscoveryRecord
	EADPubKeyAddress   *btcutil.AddressPubKeyHash
	EADExchangeFee     btcutil.Amount
	EADMultiSig        *crypto.MultiSigAddress

	// EAD responses
	RespInitExchange    *msg.ResponseInitExchange
	MaxBlockHeight      uint64
	RespClientLockFunds *msg.ResponseClientFundsLockTx
	CrossShardTx        *txmodels.SwapTransaction
	EADLockTxHash       string
	EADLockTxOutNum     uint64

	// TxMan config
	TxMan *txutils.TxMan

	RefundingTxPath string
}

type CrossShard3SigTxContext struct {
	CrossShardTXContext

	// Address which can be used to discover receiver's p2p service.
	ReceiverDiscoveryAddress string

	ReceiverPubKey  *btcec.PublicKey
	ReceiverAddress *btcutil.AddressPubKeyHash
	ReceiverTxUUID  uuid.UUID
}

type EADAddress struct {
	IP      net.IP
	Port    int64
	ExpDate int64
	// todo : use shards.ID after txscript.EADScriptData.ShardID have the same type
	Shards []uint32
}

type EADRegistrationTxContext struct {
	TxContext
	Addresses []EADAddress
	TxMan     *txutils.TxMan

	TxHash *chainhash.Hash
}

type RefundingTxContext struct {
	TxContext
	ShardID            shards.ID
	RefundingTx        []byte
	RefundingLokWindow uint64

	TxMan  *txutils.TxMan
	TxHash *chainhash.Hash
}
