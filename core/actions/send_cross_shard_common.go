package actions

import (
	"bufio"
	"bytes"
	"client/core/actions/context"
	"client/core/actions/cross_shard"
	"client/core/discovery"
	"client/core/keychain"
	"client/core/logger"
	"client/core/settings"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/jaxnet/core/indexer/clients/utxos/biggest_first"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txmodels"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"gitlab.com/jaxnet/core/shard.core/types/chainhash"
	"gitlab.com/jaxnet/core/shard.core/types/wire"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/messages/clients_messages"
	"io/ioutil"
	"math/big"
	"net"
	"net/http"
	"os"
	"path"
	"strings"
	"time"
)

var (
	ErrEADRejectedExchangeOperation = errors.New("EAD rejected exchange operation")
)

func confirmExchangeFee(ctx *context.CrossShardTXContext, scanner *bufio.Scanner) (confirm bool, err error) {
	fmt.Print(fmt.Sprintf("Exchange fee is equal %s. Confirm? [Y/n]:", ctx.EADExchangeFee.String()))
	scanner.Scan()
	answer := scanner.Text()
	answer = strings.ToLower(strings.TrimSpace(answer))
	confirm = answer == "" || answer == "y" || answer == "yes"
	return
}

func confirmNetworkFee(txMan *txutils.TxMan, shardID shards.ID, sourceAddress string, txAmount btcutil.Amount, scanner *bufio.Scanner,
	clientIndexer *biggest_first.UTXOProvider) (confirm bool, chosenFee btcutil.Amount, updatedAmount btcutil.Amount, err error) {
	fees, err := txMan.RPC().GetExtendedFee()
	if err != nil {
		return
	}

	var prevFastFee int64
	var prevUtxosCnt int
	for {

		indexerResponse, err := clientIndexer.SelectForAmountWithoutLock(
			int64(txAmount)+prevFastFee, uint32(shardID), sourceAddress)
		if err != nil {
			return false, 0, 0, err
		}

		var utxosSum int64
		for _, utxo := range indexerResponse {
			utxosSum += utxo.Value
		}

		prevFastFee = txutils.EstimateFee(len(indexerResponse), 1, int64(fees.Fast.SatoshiPerB), true)
		if utxosSum >= int64(txAmount)+prevFastFee {
			slowFee := btcutil.Amount(txutils.EstimateFee(len(indexerResponse), 1, int64(fees.Slow.SatoshiPerB), true))
			moderateFee := btcutil.Amount(txutils.EstimateFee(len(indexerResponse), 1, int64(fees.Moderate.SatoshiPerB), true))
			fastFee := btcutil.Amount(txutils.EstimateFee(len(indexerResponse), 1, int64(fees.Fast.SatoshiPerB), true))
			totalAmountWithLowFee := txAmount + slowFee
			totalAmountWithMiddleFee := txAmount + moderateFee
			totalAmountWithHighFee := txAmount + fastFee
			fmt.Print(fmt.Sprintf("[1] Slow network fee is equal %d. Blocks count waiting is %d. Total amount %d.\n"+
				"[2] Moderate network fee is equal %d. Blocks count waiting is %d. Total amount %d.\n"+
				"[3] Fast network fee is equal %d. Blocks count waiting is %d. Total amount %d.\n"+
				"Please choose one of them: ", slowFee, fees.Slow.Blocks, totalAmountWithLowFee,
				moderateFee, fees.Moderate.Blocks, totalAmountWithMiddleFee, fastFee, fees.Fast.Blocks, totalAmountWithHighFee))
			scanner.Scan()
			answer := scanner.Text()
			answer = strings.ToLower(strings.TrimSpace(answer))
			confirm = answer == "1" || answer == "2" || answer == "3"
			switch answer {
			case "1":
				chosenFee = slowFee
			case "2":
				chosenFee = moderateFee
			case "3":
				chosenFee = fastFee
			}
			updatedAmount = txAmount
			break
		} else if prevUtxosCnt == len(indexerResponse) {
			slowFee := btcutil.Amount(txutils.EstimateFee(len(indexerResponse), 1, int64(fees.Slow.SatoshiPerB), true))
			moderateFee := btcutil.Amount(txutils.EstimateFee(len(indexerResponse), 1, int64(fees.Moderate.SatoshiPerB), true))
			fastFee := btcutil.Amount(txutils.EstimateFee(len(indexerResponse), 1, int64(fees.Fast.SatoshiPerB), true))
			var updatedSlowAmount, updatedModerateAmount, updatedFastAmount btcutil.Amount
			if btcutil.Amount(utxosSum) >= txAmount+slowFee {
				updatedSlowAmount = txAmount
			} else {
				updatedSlowAmount = btcutil.Amount(utxosSum) - slowFee
			}
			if btcutil.Amount(utxosSum) >= txAmount+moderateFee {
				updatedModerateAmount = txAmount
			} else {
				updatedModerateAmount = btcutil.Amount(utxosSum) - moderateFee
			}
			if btcutil.Amount(utxosSum) >= txAmount+fastFee {
				updatedFastAmount = txAmount
			} else {
				updatedFastAmount = btcutil.Amount(utxosSum) - fastFee
			}
			totalAmountWithLowFee := updatedSlowAmount + slowFee
			totalAmountWithMiddleFee := updatedModerateAmount + moderateFee
			totalAmountWithHighFee := updatedFastAmount + fastFee
			fmt.Print(fmt.Sprintf("You don't have enough funds for this transaction. You can send next variants: \n"+
				"[1] Slow network fee is equal %d. New tx amount is %d. Blocks count waiting is %d. Total amount %d.\n"+
				"[2] Moderate network fee is equal %d. New tx amount is %d. Blocks count waiting is %d. Total amount %d.\n"+
				"[3] Fast network fee is equal %d. New tx amount is %d. Blocks count waiting is %d. Total amount %d.\n"+
				"Please choose one of them: ", slowFee, updatedSlowAmount, fees.Slow.Blocks, totalAmountWithLowFee,
				moderateFee, updatedModerateAmount, fees.Moderate.Blocks, totalAmountWithMiddleFee,
				fastFee, updatedFastAmount, fees.Fast.Blocks, totalAmountWithHighFee))
			scanner.Scan()
			answer := scanner.Text()
			answer = strings.ToLower(strings.TrimSpace(answer))
			confirm = answer == "1" || answer == "2" || answer == "3"
			switch answer {
			case "1":
				chosenFee = slowFee
				updatedAmount = updatedSlowAmount
			case "2":
				chosenFee = moderateFee
				updatedAmount = updatedModerateAmount
			case "3":
				chosenFee = fastFee
				updatedAmount = updatedFastAmount
			}
			break
		} else {
			prevUtxosCnt = len(indexerResponse)
		}
	}
	return
}

func confirmRefundTxPath(ctx *context.CrossShardTXContext, scanner *bufio.Scanner) (err error) {
	dirname, err := os.UserHomeDir()
	if err != nil {
		logger.Log.Debug().Err(err).Msg("Can't get user home dir")
		dirname = ""
	}
	for {
		if dirname != "" {
			fmt.Print(fmt.Sprintf("Set path to the refunding transaction. Press Enter in case of " + dirname + " or type path : "))
		} else {
			fmt.Print(fmt.Sprintf("Set path to the refunding transaction : "))
		}
		scanner.Scan()
		answer := scanner.Text()
		answer = strings.TrimSpace(answer)
		if answer != "" {
			ctx.RefundingTxPath = answer
		} else {
			if dirname == "" {
				fmt.Println("Path can't be empty. Try again")
				continue
			} else {
				ctx.RefundingTxPath = dirname
			}
		}

		_, err = os.Create(path.Join(ctx.RefundingTxPath, "test"))
		if err != nil {
			fmt.Println("Path " + answer + " is not valid. Please try again")
			continue
		}
		err = os.Remove(path.Join(ctx.RefundingTxPath, "test"))
		break
	}
	return
}

func discoverAvailableExchangeProposals(ctx *context.CrossShardTXContext) (err error) {
	logger.Info().Msg("Looking for exchange proposals available...")

	ctx.Proposal, ctx.EADDiscoveryRecord, ctx.EADPubKeyAddress, ctx.EADExchangeFee, err =
		discovery.DiscoverBestExchangeOffer(ctx.Amount, ctx.Shards)
	if err != nil {
		logger.Error(err).Msg("Can't discover available exchange proposals due to an error")
		return
	}

	logger.Log.Info().Uint64(
		"ead", ctx.Proposal.ExchangeAgentID).Int64(
		"fee-fixed", ctx.Proposal.FeeFixedAmount.Int64()).Float64(
		"fee-percents", ctx.Proposal.FeePercents).Msg(
		"OK: Exchange proposal found")
	return
}

// todo: [refactor] Replace with common netstack.
func ConnectToEAD(ctx *context.CrossShardTXContext) (conn net.Conn, err error) {
	logger.Debug().Msg("Connecting to the EAD")

	// todo: [post PoC] Use all possible connections credentials, until successful connection.
	//		 ("discovery" provides several possible ways to connect to the EA).

	if len(ctx.EADDiscoveryRecord.Addresses) == 0 {
		err = errors.New("at least one address must be provided (poc stage limitation)")
		return
	}

	for compressedAddress, _ := range ctx.EADDiscoveryRecord.Addresses {
		conn, err = net.Dial("tcp", compressedAddress)
		if err != nil {
			continue
		}

		// Setting protocol communication version.
		bytesWritten, err1 := conn.Write([]byte{clients_messages.DefaultProtocolVersion})
		if err1 != nil || bytesWritten != clients_messages.ProtocolVersionFieldSize {
			err = errors.New("can't set protocol version")
			continue
		}
		logger.Debug().Msg("OK: Connection established well")
		return
	}
	return
}

func Init2SigExchangeOperation(ctx *context.CrossShardTXContext, conn net.Conn) (err error) {
	reqInitExchange := &clients_messages.RequestInitExchange{
		Type:                    clients_messages.RequestInitExchangeType2Sig,
		Index:                   ctx.Shards,
		SourceAmount:            new(big.Int).SetInt64(int64(ctx.Amount)),
		DestinationAmount:       new(big.Int).SetInt64(int64(ctx.Amount - ctx.EADExchangeFee)),
		DestinationShardAddress: ctx.DestShardDestAddress.(*btcutil.AddressPubKeyHash),
		SenderPubKey:            ctx.SenderPubKey,
	}

	return initExchangeOperation(reqInitExchange, ctx, conn)
}

func Init3SigExchangeOperation(ctx *context.CrossShard3SigTxContext, conn net.Conn) (err error) {
	reqInitExchange := &clients_messages.RequestInitExchange{
		Type:                    clients_messages.RequestInitExchangeType3Sig,
		Index:                   ctx.Shards,
		SourceAmount:            new(big.Int).SetInt64(int64(ctx.Amount)),
		DestinationAmount:       new(big.Int).SetInt64(int64(ctx.Amount - ctx.EADExchangeFee)),
		DestinationShardAddress: ctx.DestShardDestAddress.(*btcutil.AddressPubKeyHash),
		SenderPubKey:            ctx.SenderPubKey,
		ReceiverPubKey:          ctx.ReceiverPubKey,
	}

	return initExchangeOperation(reqInitExchange, &ctx.CrossShardTXContext, conn)
}

func Prepare2Of2MultiSig(ctx *context.CrossShardTXContext) (err error) {

	ctx.SenderEADMultiSig, err = cross_shard.Create2Of2LockMultiSig(
		ctx.SenderPubKey, ctx.EADDiscoveryRecord.PubKey,
		int32(ctx.RespInitExchange.SourceShardMultiSigLockWindowBlocks), settings.Conf.Network)
	if err != nil {
		return
	}
	logger.Log.Debug().Str(
		"address", ctx.SenderEADMultiSig.Address).Str(
		"redeem", ctx.SenderEADMultiSig.RedeemScript).Int32(
		"refund-deferring-period", int32(ctx.RespInitExchange.SourceShardMultiSigLockWindowBlocks)).Msg(
		"Created 2/2 lock funds multi sig. Source shard")

	ctx.EADMultiSig, err = cross_shard.Create2Of2LockMultiSig(ctx.EADDiscoveryRecord.PubKey, ctx.SenderPubKey,
		int32(ctx.RespInitExchange.DestinationShardMultiSigLockWindowBlocks), settings.Conf.Network)
	if err != nil {
		return
	}
	logger.Log.Debug().Str(
		"address", ctx.EADMultiSig.Address).Str(
		"redeem", ctx.EADMultiSig.RedeemScript).Int32(
		"refund-deferring-period", int32(ctx.RespInitExchange.DestinationShardMultiSigLockWindowBlocks)).Msg(
		"Created 2/2 lock funds multi sig. Destination shard")

	return
}

func Prepare3Of3MultiSig(ctx *context.CrossShard3SigTxContext) (err error) {

	ctx.SenderEADMultiSig, err = cross_shard.Create3Of3LockMultiSig(
		ctx.SenderPubKey, ctx.ReceiverPubKey, ctx.EADDiscoveryRecord.PubKey,
		int32(ctx.RespInitExchange.SourceShardMultiSigLockWindowBlocks), settings.Conf.Network)

	if err != nil {
		return
	}

	logger.Log.Debug().Str(
		"address", ctx.SenderEADMultiSig.Address).Str(
		"redeem", ctx.SenderEADMultiSig.RedeemScript).Int32(
		"refund-deferring-period", int32(ctx.RespInitExchange.SourceShardMultiSigLockWindowBlocks)).Msg(
		"Created 3/3 lock funds multi sig. Source shard")

	ctx.EADMultiSig, err = cross_shard.Create3Of3LockMultiSig(
		ctx.EADDiscoveryRecord.PubKey, ctx.SenderPubKey, ctx.ReceiverPubKey,
		int32(ctx.RespInitExchange.DestinationShardMultiSigLockWindowBlocks), settings.Conf.Network)
	if err != nil {
		return
	}

	logger.Log.Debug().Str(
		"address", ctx.EADMultiSig.Address).Str(
		"redeem", ctx.EADMultiSig.RedeemScript).Int32(
		"refund-deferring-period", int32(ctx.RespInitExchange.DestinationShardMultiSigLockWindowBlocks)).Msg(
		"Created 3/3 lock funds multi sig. Destination shard")

	return
}

func LockFundsInMultiSigAddressAndInformEADAboutLock(ctx *context.CrossShardTXContext, conn net.Conn,
	clientIndexer *biggest_first.UTXOProvider) (err error) {

	_, previousBlockNumber, err := ctx.TxMan.RPC().GetBestBlock()
	if err != nil {
		return
	}

	waitNextBlock := func(currentBlock int32) (blockNumber int32, err error) {
		for {
			time.Sleep(time.Second * 5)
			_, blockNumber, err = ctx.TxMan.RPC().GetBestBlock()
			if err != nil {
				return
			}
			if blockNumber > previousBlockNumber {
				return
			}
		}
	}

	var txHash *chainhash.Hash
	var tx *wire.MsgTx
	currentAttempt := 1

	for {
		txBuilder := txutils.NewTxBuilder(chaincfg.NetName(settings.Conf.Blockchain.NetworkName)).
			SetSenders(ctx.SenderAddress.EncodeAddress()).
			SetShardID(uint32(ctx.Shards.SourceShard)).
			SetUTXOProvider(clientIndexer).
			SetDestination(ctx.SenderEADMultiSig.Address, int64(ctx.Amount)).
			SetChangeDestination(ctx.SenderAddress.EncodeAddress())
		tx, err = txBuilder.IntoTx(func(intShardID uint32) (int64, int64, error) {
			return int64(ctx.SenderFundsLockFee), 0, nil
		}, keychain.KeyChain.KeyDada())
		if err != nil {
			return
		}

		err = generateAndSaveRefundTxInfo(tx, ctx)
		if err != nil {
			return
		}

		logger.Log.Info().Msg("Preparing the lock funds transaction")

		txHash, err = ctx.TxMan.ForShard(uint32(ctx.Shards.SourceShard)).RPC().SendRawTransaction(tx)
		if err == nil {
			logger.Log.Debug().Str(
				"hash", txHash.String()).Msg(
				"Funds locking tx sent to blockchain")
			break
		}

		logger.Log.Debug().Err(err).Msg("Can't send transaction to blockchain")

		if currentAttempt == LockFundsMaxCountAttempts {
			err = errors.New("Max count of failed attempts sending transaction to blockchain " + err.Error())
			return
		}
		currentAttempt++
		previousBlockNumber, err = waitNextBlock(previousBlockNumber)
		if err != nil {
			return
		}
	}

	logger.Log.Debug().Msg("Checking funds lock tx to be mined")
	for {
		time.Sleep(time.Second * 5)

		_, err := ctx.TxMan.ForShard(uint32(ctx.Shards.SourceShard)).RPC().GetRawTransaction(txHash, false)
		if err != nil {
			logger.Log.Err(err).Msg("No tx found in blockchain")
		}

		out, err := ctx.TxMan.ForShard(uint32(ctx.Shards.SourceShard)).RPC().GetTxOut(txHash, 0, true, false)
		if err != nil {
			return err
		}

		if out == nil {
			logger.Log.Debug().Msg("No lock funds TX found in chain. Waiting...")
			continue
		}

		if out.Confirmations < LockFundsRequiredConfirmationsCnt {
			logger.Log.Debug().Int64(
				"confirmations", out.Confirmations).Msg("Lock funds TX found, but not enough confirmations")

			continue
		}

		logger.Log.Debug().Msg("Lock TX found and mined with enough confirmations")
		break
	}

	// Storing funds lock tx is needed for further validation.
	// See Validate3SigCrossShardTx() for the details.
	ctx.SenderFundsLockTX = tx

	reqClientFundsLockTx := &clients_messages.RequestClientFundsLockTx{
		TxUUID:               ctx.RespInitExchange.TxUUID,
		TxHash:               txHash[:],
		MultiSigRedeemScript: ctx.SenderEADMultiSig.RawRedeemScript,
	}

	err = sendRequest(conn, reqClientFundsLockTx)
	if err != nil {
		return
	}

	logger.Log.Debug().Msg(
		"Client funds lock request sent to the EAD")

	// Wait for EAD to process the request.
	time.Sleep(time.Second * 3)

	resp, err := ReceiveResponse(conn)
	if err != nil {
		return
	}

	ctx.RespClientLockFunds = resp.(*clients_messages.ResponseClientFundsLockTx)
	logger.Log.Debug().Uint8(
		"code", ctx.RespClientLockFunds.Code).Msg(
		"Client funds lock response received")

	if ctx.RespClientLockFunds.Code != 0 {
		err = errors.New("EA has rejected the funds lock tx")
		return
	}

	//// todo: [post PoC] Sleep some time for transaction to be executed on chain.
	////		 There is no any reason to poll EA until transaction would not be executed on chain,
	////		 so it would be nice to have a time sleep here for average TX execution time.
	//
	//// todo: [post PoC] This operation could take fow a while,
	//// 		 so current TCP connection could be closed, and new one should be used for further communications.
	//
	//// start polling for EAD funds to be locked
	//for i := 0; i < 120; i++ {
	//	time.Sleep(time.Second)
	//	err = PollEadFundsLocked(ctx)
	//	if err != nil {
	//		logger.Log.Err(err).Msg(
	//			"Can't check if EA has locked funds. Going to check once more after one second")
	//		continue
	//
	//	} else {
	//		break
	//	}
	//}
	//if err != nil {
	//	return
	//}
	//
	//if ctx.RespEADLockFunds.Code != clients_messages.OK {
	//	// todo: [Post PoC]
	//	//		 * Wait for time lock to unlock funds.
	//	// 		 * Move funds from the multi sig. back.
	//
	//	err = errors.New("EAD has not locked funds, terminating the operation")
	//	return
	//}

	return
}

func FetchCrossShardTxFromEAD(ctx *context.CrossShardTXContext) (err error) {

	request := &clients_messages.RequestFetchCrossShardTx{
		TxUUID: ctx.TxUUID,
	}

	logger.Log.Info().Msg("Fetching cross shard TX from the EAD")

	nonCriticalFailsCount := 0
	maxNonCriticalFailsCount := 100

	for {
		_, currentBlockNumber, err := ctx.TxMan.ForShard(
			uint32(ctx.Shards.SourceShard)).RPC().GetBestBlock()
		if err != nil {
			nonCriticalFailsCount += 1
			if nonCriticalFailsCount >= maxNonCriticalFailsCount {
				err = fmt.Errorf("Can't get block number from blockchain: %w", err)
				return err
			}
			time.Sleep(time.Second * FetchCrossShardTxFromEADRetryDelayInSec)
			continue
		}
		if uint64(currentBlockNumber) > ctx.MaxBlockHeight {
			err = errors.New("Transaction is elapsed now")
			return err
		}

		conn, err := ConnectToEAD(ctx)
		if err != nil {
			logger.Log.Debug().Err(err).Msg("Can't connect to the EAD. Try later again")
			time.Sleep(time.Second * FetchCrossShardTxFromEADRetryDelayInSec)
			continue
		}

		err = sendRequest(conn, request)
		if err != nil {
			logger.Log.Debug().Err(err).Msg("Can't request cross shard TX from the EAD. Try later again")
			time.Sleep(time.Second * FetchCrossShardTxFromEADRetryDelayInSec)
			continue
		}

		resp, err := ReceiveResponse(conn)
		if err != nil {
			logger.Log.Debug().Err(err).Msg("Can't fetch response on cross shard TX from the EAD. Try later again")
			time.Sleep(time.Second * FetchCrossShardTxFromEADRetryDelayInSec)
			continue
		}

		response, ok := resp.(*clients_messages.ResponseFetchCrossShardTx)
		if !ok {
			msg := "can't fetch response on cross shard TX from the EAD: EAD responded with unsupported response"
			err = errors.New(msg)
			return err
		}

		if response.Code == clients_messages.InProgress {
			logger.Log.Info().Msg("EAD reports that TX is still in progress...")
			time.Sleep(time.Second * 3)
			continue
		}

		if response.Code == clients_messages.TransactionAborted {
			// todo: ensure fallback logic execution to unlock funds here.
			err = errors.New("EAD reports that transaction has been aborted")
			logger.Log.Err(err).Msg("")
			return err
		}

		if response.Code == clients_messages.OK {
			logger.Log.Info().Msg("OK: Fetched cross shard TX from the EAD.")

			ctx.CrossShardTx = &txmodels.SwapTransaction{}
			err = ctx.CrossShardTx.UnmarshalBinary(response.CrossShardTx)
			if err != nil {
				// todo: ensure fallback logic execution to unlock funds here.
				err = fmt.Errorf("can't unmarshall cross shard TX, data seems to be corruped")
				return err
			}

			ctx.EADLockTxHash = response.LockTxHash
			ctx.EADLockTxOutNum = response.LockTxOutNum

			logger.Log.Info().Msg("OK: Successfully unmarshalled the received TX")
			return err
		}

		// Unexpected response from the EAD with the code we do not expect.
		// todo: ensure fallback logic execution to unlock funds here.
		msg := "EAD responds with the code that is unexpected and could not be processed. " +
			"Transaction execution would be aborted"
		err = errors.New(msg)
		logger.Log.Err(err).Msg("")
		return err
	}
}

func SignCrossShardTX(ctx *context.CrossShardTXContext) (err error) {

	var swapTxWithMultiSig *wire.MsgTx
	swapTxWithMultiSig, err = ctx.TxMan.AddSignatureToSwapTx(ctx.CrossShardTx.RawTX,
		[]uint32{uint32(ctx.Shards.SourceShard), uint32(ctx.Shards.DestinationShard)},
		ctx.SenderEADMultiSig.RedeemScript, ctx.EADMultiSig.RedeemScript)

	if err != nil {
		return
	}

	ctx.CrossShardTx.RawTX = swapTxWithMultiSig
	ctx.CrossShardTx.SignedTx = txutils.EncodeTx(swapTxWithMultiSig)
	ctx.CrossShardTx.TxHash = swapTxWithMultiSig.TxHash().String()

	logger.Log.Info().Str(
		"hash", ctx.CrossShardTx.TxHash).Str(
		"signed-tx", ctx.CrossShardTx.SignedTx).Str(
		"source", ctx.CrossShardTx.Source).Msg(
		"Signed cross-Shard TX (previously received from EAD)")

	return
}

//func PollEadFundsLocked(ctx *context.CrossShardTXContext) (err error) {
//	conn, err := ConnectToEAD(ctx)
//	if err != nil {
//		return
//	}
//
//	reqEADFundsLockTx := &clients_messages.RequestEADFundsLockTx{
//		TxUUID: ctx.RespInitExchange.TxUUID,
//	}
//
//	err = sendRequest(conn, reqEADFundsLockTx)
//	if err != nil {
//		return
//	}
//
//	resp, err := ReceiveResponse(conn)
//	if err != nil {
//		return
//	}
//
//	response := resp.(*clients_messages.ResponseEADFundsLockTx)
//
//	txHash, err := chainhash.NewHash(response.TxHash)
//	if err != nil {
//		return
//	}
//
//	out, err := ctx.TxMan.ForShard(uint32(ctx.Shards.DestinationShard)).RPC().GetTxOut(txHash, 0, false)
//	if err != nil {
//		return
//	}
//
//	if out == nil {
//		logger.Log.Info().Msg("EAD funds lock TX not found")
//		err = errors.New("EAD funds lock TX not found")
//		return
//	}
//
//	if out.Confirmations < 6 { // todo: get confirmations amount from settings or command arguments
//		logger.Log.Info().Int64(
//			"confirmations", out.Confirmations).Msg(
//			"EAD funds lock TX is found, but has not enough confirmations")
//
//		err = errors.New("not enough confirmations")
//		return
//
//	} else {
//		ctx.RespEADLockFunds = resp.(*clients_messages.ResponseEADFundsLockTx)
//	}
//
//	return
//}

func ValidateEADFundsLock() (err error) {
	// todo: [security] check that lock tx is executed.
	// todo: [security] check that it spends funds into the specified multi sig address AND
	// 		 that it is executed in destination shard.
	return
}

func initExchangeOperation(req *clients_messages.RequestInitExchange, ctx *context.CrossShardTXContext, conn net.Conn) (err error) {

	// ToDo: Remove this check when RequestInitExchange will be updated
	// 		 to support both btcutil.AddressPubKeyHash and btcutil.AddressScriptHash.
	_, isDestAddressPubKeyHash := ctx.DestShardDestAddress.(*btcutil.AddressPubKeyHash)
	if !isDestAddressPubKeyHash {
		err = errors.New("" +
			"at the moment, destination address could be only of type address pub key hash, " +
			"(multi sig addresses are not supported yet)")
		return
	}

	err = sendRequest(conn, req)
	if err != nil {
		return
	}

	// Wait for EAD to process the request.
	time.Sleep(time.Second)

	resp, err := ReceiveResponse(conn)
	if err != nil {
		return
	}

	ctx.RespInitExchange = resp.(*clients_messages.ResponseInitExchange)
	if ctx.RespInitExchange.Code != 0 {
		err = ErrEADRejectedExchangeOperation
		return
	}

	_, currentBlockNumber, err := ctx.TxMan.ForShard(
		uint32(ctx.Shards.SourceShard)).RPC().GetBestBlock()
	if err != nil {
		return
	}
	ctx.MaxBlockHeight = uint64(currentBlockNumber) + ctx.RespInitExchange.SourceShardMultiSigLockWindowBlocks

	if ctx.RespInitExchange.SourceShardMultiSigLockWindowBlocks < LockFundsRequiredConfirmationsCnt {

		err = errors.New("EA has proposed to short multi sig lock time window")
		logger.Log.Err(err).Uint64(
			"source-shard-lockup-blocks", ctx.RespInitExchange.SourceShardMultiSigLockWindowBlocks).Uint64(
			"destination-shard-lockup-blocks", ctx.RespInitExchange.DestinationShardMultiSigLockWindowBlocks).Msg(
			"")

		return
	}

	ctx.TxUUID = ctx.RespInitExchange.TxUUID
	// todo : WARNING validate lock window blocks. add them to processing. change logic (from relevant to absolute) in refunding tx
	logger.Log.Info().Uint64(
		"source-shard-lockup-blocks", ctx.RespInitExchange.SourceShardMultiSigLockWindowBlocks).Uint64(
		"destination-shard-lockup-blocks", ctx.RespInitExchange.DestinationShardMultiSigLockWindowBlocks).Msg(
		"EAD requested next locking configuration")

	return
}

func generateAndSaveRefundTxInfo(tx *wire.MsgTx, ctx *context.CrossShardTXContext) (err error) {
	if len(tx.TxOut) > 2 {
		err = errors.New("Transaction has more than one output")
		return
	}

	fee, err := ctx.TxMan.RPC().ForShard(uint32(ctx.Shards.SourceShard)).GetExtendedFee()
	if err != nil {
		return
	}
	moderateFee := txutils.EstimateFee(1, 1,
		int64(fee.Moderate.SatoshiPerB), true)

	out := tx.TxOut[0]
	txRefund, err := txutils.NewTxBuilder(chaincfg.NetName(ctx.TxMan.NetParams.Name)).
		SetSenders(ctx.SenderEADMultiSig.Address).
		AddRedeemScripts(ctx.SenderEADMultiSig.RedeemScript).
		SetDestinationWithUTXO(ctx.SenderAddress.EncodeAddress(), int64(ctx.Amount)-moderateFee, txmodels.UTXORows{{
			ShardID:  uint32(ctx.Shards.SourceShard),
			Address:  ctx.SenderEADMultiSig.Address,
			TxHash:   tx.TxHash().String(),
			Value:    out.Value,
			PKScript: hex.EncodeToString(out.PkScript),
		}}).
		IntoTx(func(shardID uint32) (int64, int64, error) { return moderateFee, 0, nil }, keychain.KeyChain.KeyDada())
	if err != nil {
		return
	}

	fileName := fmt.Sprintf("%s:%d", txRefund.TxHash().String(), ctx.Amount)
	file, err := os.Create(path.Join(ctx.RefundingTxPath, fileName))
	if err != nil {
		return
	}
	defer file.Close()

	buf := new(bytes.Buffer)
	err = txRefund.Serialize(buf)
	if err != nil {
		return
	}

	_, err = file.WriteString(fmt.Sprintf("%s\n%d\n%d\n", hex.EncodeToString(buf.Bytes()),
		ctx.Shards.SourceShard,
		ctx.MaxBlockHeight))
	if err != nil {
		return
	}

	logger.Log.Info().Str("file-name", path.Join(ctx.RefundingTxPath, fileName)).
		Msg("Refunding transaction created and store into file")
	return
}

func CheckEADLockTx(shardID shards.ID, lockTxHash string, txOutputNum uint64, multiSigAddress string,
	expectedAmount btcutil.Amount, txMan *txutils.TxMan) (err error) {

	lockTxMined, err := CheckLockTxMined(shardID, lockTxHash, txOutputNum, txMan)
	if err != nil {
		return
	}
	if !lockTxMined {
		err = errors.New("Lock tx not mined")
		return
	}

	amountCorrect, err := CheckLockAmountAndDestination(shardID, lockTxHash, txOutputNum, expectedAmount, multiSigAddress, txMan)
	if err != nil {
		return
	}
	if !amountCorrect {
		err = errors.New("Lock tx amount or destination is not refer to exchange")
		return
	}

	isUsed, err := CheckIfEADLockDidntUseYet(shardID, multiSigAddress, lockTxHash, txOutputNum, txMan)
	if err != nil {
		return
	}
	if isUsed {
		err = errors.New("EAD lock transaction is already used")
		return
	}

	return
}

func CheckIfEADLockDidntUseYet(shardID shards.ID, multiSigAddress string, lockTxHash string, lockTxOutIdx uint64,
	txMan *txutils.TxMan) (isUsed bool, err error) {

	shardConf, ok := settings.Conf.ShardsMap[shardID]
	if !ok {
		err = errors.New("There are no addresses txs indexer config for shard")
		return
	}
	requestUrl := fmt.Sprintf(
		"%s/api/v1/indexes/addresses/transactions/with-input/?address=%s&tx_input=%s&tx_input_idx=%d",
		shardConf.AddressesTxsIndexerInterface, multiSigAddress, lockTxHash, lockTxOutIdx)

	resp, err := http.Get(requestUrl)
	if err != nil {
		return
	}
	if resp.Status != "200 OK" {
		err = errors.New("Wrong response from payments handler")
		return
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var addressesTxs []string
	err = json.Unmarshal(bodyBytes, &addressesTxs)
	if err != nil {
		return
	}

	isUsed = len(addressesTxs) > 0

	return
}

func CheckLockAmountAndDestination(shardID shards.ID, lockTxHash string, lockTxOutIdx uint64,
	expectedAmount btcutil.Amount, expectedAddress string, txMan *txutils.TxMan) (check bool, err error) {
	lockTxHashHash, err := chainhash.NewHashFromStr(lockTxHash)
	if err != nil {
		return
	}
	transaction, err := txMan.RPC().ForShard(uint32(shardID)).GetRawTransaction(lockTxHashHash, false)
	if err != nil {
		return false, err
	}
	if len(transaction.MsgTx().TxOut) <= int(lockTxOutIdx) {
		err = errors.New("Lock TX output idx is invalid")
		return
	}
	check = transaction.MsgTx().TxOut[int(lockTxOutIdx)].Value == int64(expectedAmount)
	if !check {
		return
	}
	decodedOutput, err := txutils.DecodeScript(transaction.MsgTx().TxOut[int(lockTxOutIdx)].PkScript, txMan.NetParams)
	if err != nil {
		return
	}

	check = decodedOutput.Addresses[0] == expectedAddress
	return
}

func CheckLockTxMined(shardID shards.ID, lockTxHash string, txOutputNum uint64, txMan *txutils.TxMan) (mined bool, err error) {
	lockTxHashHash, err := chainhash.NewHashFromStr(lockTxHash)
	if err != nil {
		return
	}
	mined = false
	cntCriticalAttempts := 5
	attempt := 1

	for {
		out, err := txMan.ForShard(uint32(shardID)).RPC().GetTxOut(
			lockTxHashHash, uint32(txOutputNum), true, false)
		if err != nil {
			return false, err
		}

		if out == nil {
			if attempt <= cntCriticalAttempts {
				logger.Log.Debug()
				attempt++
				time.Sleep(time.Second * RegularTxRoundDelaySeconds)
				continue
			}
			return false, nil
		}

		if out.Confirmations <= LockFundsRequiredConfirmationsCnt {
			logger.Log.Debug().Str("hash", lockTxHash).Int64(
				"confirmations", out.Confirmations).Msg(
				"EAD lock TX found, but not enough confirmations")
			time.Sleep(time.Second * RegularTxRoundDelaySeconds)
			continue
		}
		break
	}
	mined = true
	return
}
