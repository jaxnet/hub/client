package actions

import (
	"bufio"
	"client/core/actions/context"
	"client/core/keychain"
	"client/core/logger"
	"client/core/settings"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/common/netstack/implementation/messages"
	"gitlab.com/jaxnet/common/netstack/implementation/protocols/p2p/cross_shard_3sig"
	"gitlab.com/jaxnet/common/netstack/stack/communicators"
	"gitlab.com/jaxnet/common/netstack/stack/ec"
	m "gitlab.com/jaxnet/common/netstack/stack/messages/common"
	"gitlab.com/jaxnet/common/netstack/stack/proto"
	"gitlab.com/jaxnet/core/indexer/clients/utxos/biggest_first"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"os"
)

func processSenderEAReceiverTx(cli *cli.Context, ctx *context.CrossShard3SigTxContext) (err error) {
	err = parseSenderEAReceiverTxArguments(cli, ctx)
	if err != nil {
		return
	}

	shardConf, ok := settings.Conf.ShardsMap[ctx.Shards.SourceShard]
	if !ok {
		err = errors.New("There are no utxos indexer config for shard")
		return
	}

	clientIndexer, err := biggest_first.New(shardConf.NonSpentUtxosIndexerHostAndPort,
		false, keychain.KeyChain.SignDataForIndexer)
	if err != nil {
		return
	}

	logger.Log.Info().Int64(
		"amount", int64(ctx.Amount)).Uint32(
		"source-shard-id", uint32(ctx.Shards.SourceShard)).Uint32(
		"destination-shard-id", uint32(ctx.Shards.DestinationShard)).Str(
		"receiver-pub-key", hex.EncodeToString(ctx.ReceiverPubKey.SerializeUncompressed())).Msg(
		"Cross-chain transaction started using S->EA->R schema (3 sig)")

	//
	// Protocol handlers
	//

	handleProposalsDiscovering := func(flowCtx *proto.Context, msg m.Message) (
		nextState *proto.State, outgoingMessage m.Message, err error) {

		err = discoverAvailableExchangeProposals(&ctx.CrossShardTXContext)
		if err != nil {
			return
		}

		scanner := bufio.NewScanner(os.Stdin)
		confirm, err := confirmExchangeFee(&ctx.CrossShardTXContext, scanner)
		if err != nil {
			return
		}
		if !confirm {
			return
		}

		confirm, ctx.SenderFundsLockFee, ctx.Amount, err = confirmNetworkFee(
			ctx.TxMan.ForShard(uint32(ctx.Shards.SourceShard)), ctx.Shards.SourceShard,
			ctx.SenderAddress.EncodeAddress(), ctx.Amount, scanner, clientIndexer)
		if err != nil {
			return
		}
		if !confirm {
			return
		}

		err = confirmRefundTxPath(&ctx.CrossShardTXContext, scanner)
		if err != nil {
			return
		}

		nextState = cross_shard_3sig.SenderSetTxContext
		return
	}

	// handleSetTxContext establishes the common TX context with the Receiver.
	// The goal of this stage is to inform the Receiver about incoming payment
	// and to receive common TxUUID for this payment.
	handleSetTxContext := func(flowCtx *proto.Context, msg m.Message) (
		nextState *proto.State, outgoingMessage m.Message, err error) {

		logger.Log.Debug().Msg("Establishing TX context with the receiver")
		nextState = cross_shard_3sig.SenderCheckTxContextResponse
		outgoingMessage = &messages.InitCrossShard3SigPayment{
			SourceShardID:      uint32(ctx.Shards.SourceShard),
			DestinationShardID: uint32(ctx.Shards.DestinationShard),
			SenderPubKey:       ctx.SenderPubKey,
			EADPubKey:          ctx.EADDiscoveryRecord.PubKey,
			Amount:             ctx.Amount - ctx.EADExchangeFee,
		}

		return
	}

	handleSetTxContextResponse := func(flowCtx *proto.Context, msg m.Message) (
		nextState *proto.State, outgoingMessage m.Message, err error) {

		response := msg.(*messages.CrossShardInit3SigPaymentResponse)
		if response.Code != messages.CodeOK {
			record := fmt.Sprint("Receiver responded with reject. Received code - ", response.Code, " %w")
			receiverError := fmt.Errorf(record, ec.ErrProtocolError)
			logger.Error(receiverError)

			nextState = proto.FinalState
			return
		}

		logger.Info().Str(
			"tx-uuid", response.TxUUID.String()).Msg(
			"Receiver approved operation")
		ctx.ReceiverTxUUID = response.TxUUID

		nextState = cross_shard_3sig.SenderEADCrossShardTxPreparing
		return
	}

	handleEADCrossShardTxPreparing := func(flowCtx *proto.Context, msg m.Message) (
		nextState *proto.State, outgoingMessage m.Message, err error) {

		conn, err := ConnectToEAD(&ctx.CrossShardTXContext)
		if err != nil {
			return
		}

		err = Init3SigExchangeOperation(ctx, conn)
		if err != nil {
			return
		}

		err = Prepare3Of3MultiSig(ctx)
		if err != nil {
			return
		}

		err = LockFundsInMultiSigAddressAndInformEADAboutLock(&ctx.CrossShardTXContext, conn, clientIndexer)
		if err != nil {
			return
		}

		err = FetchCrossShardTxFromEAD(&ctx.CrossShardTXContext)
		if err != nil {
			return
		}

		logger.Log.Info().Str(
			"hash", ctx.CrossShardTx.RawTX.TxHash().String()).Str(
			"signed-tx", ctx.CrossShardTx.SignedTx).Str(
			"source", ctx.CrossShardTx.Source).Str(
			"ead-lock-tx-hash", ctx.EADLockTxHash).Uint64(
			"ead-lock-tx-out-num", ctx.EADLockTxOutNum).Msg(
			"Cross-Shard TX received from EAD")

		err = Validate3SigCSTX(ctx)
		if err != nil {
			return
		}

		err = SignCrossShardTX(&ctx.CrossShardTXContext)
		if err != nil {
			return
		}

		nextState = cross_shard_3sig.SenderShareCrossShardTx
		return
	}

	handleShareCrossShardTx := func(flowCtx *proto.Context, msg m.Message) (
		nextState *proto.State, outgoingMessage m.Message, err error) {

		outgoingMessage = &messages.CrossShard3SigTx{
			TxUUID:                                   ctx.ReceiverTxUUID,
			Tx:                                       ctx.CrossShardTx,
			SourceShardMultiSigLockWindowBlocks:      ctx.RespInitExchange.SourceShardMultiSigLockWindowBlocks,
			DestinationShardMultiSigLockWindowBlocks: ctx.RespInitExchange.DestinationShardMultiSigLockWindowBlocks,
			EADLockTxHash:                            ctx.EADLockTxHash,
			EADLockTxOutNum:                          ctx.EADLockTxOutNum,
		}
		nextState = cross_shard_3sig.SenderCheckCrossShardTxResponse
		return
	}

	handleShareCrossShardTxResponse := func(flowCtx *proto.Context, msg m.Message) (
		nextState *proto.State, outgoingMessage m.Message, err error) {

		message := msg.(*messages.CrossShard3SigTxResponse)
		if message.Code != messages.CodeOK {
			logger.Log.Error().Msg(
				"Receiver does not accept the cross shard TX. " +
					"There are 2 cases possible: " +
					"1. Receiver received the TX and would sign it, but some communication issued occurred." +
					"2. Receiver has not received the TX." +
					"\n\n" +
					"To cover case #1 chain monitoring is started. " +
					"If no payment would be done - the money would be get back " +
					"when the lock would be taken back.")

			// todo: begin chain monitoring.
			panic("Not implemented")
		} else {
			logger.Log.Info().Msg("Receiver has received the TX and is going to sign it")
		}

		return
	}

	protocol, err := cross_shard_3sig.SenderReceiverCrossShardPayment()
	if err != nil {
		return
	}

	communicator := communicators.NewTCPCommunicator(protocol, logger.Log.Logger)
	err = communicator.Follow(
		ctx.ReceiverDiscoveryAddress,
		[]proto.StateHandler{
			handleProposalsDiscovering,
			handleSetTxContext,
			handleSetTxContextResponse,
			handleEADCrossShardTxPreparing,
			handleShareCrossShardTx,
			handleShareCrossShardTxResponse,
		},
	)

	return
}

func parseSenderEAReceiverTxArguments(cli *cli.Context, ctx *context.CrossShard3SigTxContext) (err error) {
	ctx.ReceiverDiscoveryAddress = cli.String("receiver-resolving-address")
	receiverPubKeyHex := cli.String("receiver-pub-key")
	receiverPubKeyBinary, err := hex.DecodeString(receiverPubKeyHex)
	if err != nil {
		return
	}

	ctx.ReceiverPubKey, err = btcec.ParsePubKey(receiverPubKeyBinary, btcec.S256())
	if err != nil {
		return
	}

	receiverPubKeySerialized := ctx.ReceiverPubKey.SerializeUncompressed()
	receiverAddress, err := btcutil.NewAddressPubKey(receiverPubKeySerialized, ctx.TxMan.NetParams)
	if err != nil {
		return
	}

	ctx.ReceiverAddress = receiverAddress.AddressPubKeyHash()
	return
}
