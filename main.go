package main

import (
	"client/core/actions"
	"client/core/actions/receiver_mode"
	"client/core/ec"
	"client/core/logger"
	"client/core/settings"
	"github.com/urfave/cli/v2"
	"os"
)

func main() {
	logger.Init()

	err := settings.LoadSettings()
	ec.InterruptOnError(err)

	app := &cli.App{
		Name:  "client",
		Usage: "CLI implementation if the jax.net wallet",
		Commands: []*cli.Command{
			{
				Name:    "send",
				Aliases: []string{"s"},
				Usage:   actions.UsageSend,
				Action:  actions.Send,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     actions.FlagSourceShard,
						Aliases:  []string{"sid"},
						Usage:    "ID of the shard from which the transaction should be issued",
						Required: true,
					},
					&cli.StringFlag{
						Name:    actions.FlagDestinationShard,
						Aliases: []string{"did"},
						Usage:   "ID of the shard into which the transaction should be issued",
					},
					&cli.StringFlag{
						Name:     actions.FlagAmount,
						Aliases:  []string{"a"},
						Usage:    "amount of the operation",
						Required: true,
					},
					&cli.StringFlag{
						Name:     actions.FlagAddress,
						Aliases:  []string{"da"},
						Usage:    "address in destination shard where funds must be moved",
						Required: true,
					},
					&cli.StringFlag{
						Name:  actions.FlagReceiverResolvingAddress,
						Usage: "Resolving address for p2p communication with the receiver",
					},
					&cli.StringFlag{
						Name:  actions.FlagReceiverPubKey,
						Usage: "Pub key of the receiver",
					},
					&cli.StringFlag{
						Name:    actions.FlagPrivateKey,
						Aliases: []string{"pk"},
						Usage:   "client private key",
					},
				},
			},
			{
				Name:    "receiver-mode",
				Aliases: []string{"rm"},
				Usage:   actions.UsageReceiverMode,
				Action:  receiver_mode.ReceiverMode,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     actions.FlagPrivateKey,
						Aliases:  []string{"pk"},
						Usage:    "client private key",
						Required: true,
					},
				},
			},
			{
				Name:    "register",
				Aliases: []string{"r"},
				Usage:   actions.UsageRegister,
				Action:  actions.Register,
				Flags: []cli.Flag{
					&cli.StringSliceFlag{
						Name:    actions.FlagAddressAndShards,
						Aliases: []string{"as"},
						Usage:   "Exchange agent address and shards",
					},
					&cli.StringFlag{
						Name:    actions.FlagPrivateKey,
						Aliases: []string{"pk"},
						Usage:   "client private key",
					},
				},
			},
			{
				Name:    "get-addresses",
				Aliases: []string{"a"},
				Usage:   actions.UsageGetAddresses,
				Action:  actions.GetAddresses,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    actions.FlagPrivateKey,
						Aliases: []string{"pk"},
						Usage:   "client private key",
					},
				},
			},
			{
				Name:    "delete-address",
				Aliases: []string{"d"},
				Usage:   actions.UsageDeleteRegistration,
				Action:  actions.DeleteAddresses,
				Flags: []cli.Flag{
					&cli.StringSliceFlag{
						Name:    actions.FlagAddressAndShards,
						Aliases: []string{"as"},
						Usage:   "Exchange agent address and shards",
					},
					&cli.StringFlag{
						Name:    actions.FlagPrivateKey,
						Aliases: []string{"pk"},
						Usage:   "client private key",
					},
				},
			},
			{
				Name:    "refunding",
				Aliases: []string{"rf"},
				Usage:   actions.UsageRefunding,
				Action:  actions.Refund,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    actions.FlagRefundingFilePath,
						Aliases: []string{"f"},
						Usage:   "refunding file path",
					},
					&cli.StringFlag{
						Name:    actions.FlagRefundingTxData,
						Aliases: []string{"tx"},
						Usage:   "refunding tx data",
					},
					&cli.StringFlag{
						Name:    actions.FlagSourceShard,
						Aliases: []string{"sid"},
						Usage:   "ID of the shard on which the transaction should be refunded",
					},
					&cli.StringFlag{
						Name:    actions.FlagRefundingLockWindowBlocks,
						Aliases: []string{"lw"},
						Usage:   "refunding lock window blocks",
					},
					&cli.StringFlag{
						Name:    actions.FlagPrivateKey,
						Aliases: []string{"pk"},
						Usage:   "client private key",
					},
				},
			},
		},
	}

	err = app.Run(os.Args)
	ec.InterruptOnError(err)
}
