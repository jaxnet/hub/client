package utils

const (
	ClientExecPath   = "/home/mc/jax/client/bin/client_send_regular_tx"
	ResetEnvCmdDir   = "/home/mc/jax/infrastructure/test-env"
	ResetEnvUser     = "mykola"
	EADAddressPubKey = "041fff05a00690de368e805b73c96669b1dfc758c31cf2ecdc613fd7835b59c66fb87091" +
		"99146c5978451bc704cd2a036d938606b531fde3a1314e39b6fa8796be"

	CmdArgSend                  = "s"
	CmdArgRegister              = "r"
	CmdArgGetAddresses          = "a"
	CmdArgDeleteAddress         = "d"
	CmdArgShardIDFlag           = "-sid"
	CmdArgAmountFlag            = "-a"
	CmdArgDestAddrFlag          = "-da"
	CmdArgAddrAndShardsFlag     = "-as"
	CmdArgShardIDValCorrect     = "1"
	CmdArgShardIDValUnsupported = "1000"
	CmdArgShardIDValInvalid     = "a"
	CmdArgShardIDValNegative    = "-1"
	CmdArgShardIDValOverflow    = "1000000000000000000000000000000"
	CmdArgDestAddrValCorrect    = "muJAuq1YiEhEvgsZBS6enE1QvVTCLvKF2w"
	CmdArgDestAddrValInvalid    = "muJAuq1YiEhEvgs"
	CmdArgAmountValCorrect      = "100000000"
	CmdArgAmountValTooBig       = "10000000000000000"
	CmdArgAmountValInvalid      = "www"
	CmdArgAmountValNegative     = "-1"
	CmdArgAmountValOverflow     = "1000000000000000000000000000000"

	// Tx messages
	TxFundsSendingMsg = "Funds sending tx sent to blockchain"
	TxConfirmedMsg    = "TX found and mined with enough confirmations"
	ErrDestAddrMsg    = "can't parse destination address specified by the flag `address`: checksum mismatch"

	ErrShardIDNotMatchMsg = "Provided ShardID (" + CmdArgShardIDValUnsupported + ") does not match with any present"
	ErrShardIDInvalidMsg  = "invalid shard id:"
	ErrNotEnoughAmountMsg = "not enough amount"
	ErrInvalidAmountMsg   = "amount is required and could not be less or equal to zero"

	// EAD Registration messages
	TxRegistrationSentMsg   = "Registration tx sent to blockchain"
	TxRegistrationCreateMsg = "Registration tx created"
	ErrInvalidIPv4ValueMsg  = "Invalid IPv4 value"
	ErrInvalidIPv6ValueMsg  = "Invalid IPv6 value"
	ErrInvalidPortValueMsg  = "Invalid port value"
	ErrInvalidExpTimeMsg    = "Invalid expTimeInDays value"
	ErrInvalidShardsListMsg = "Invalid shards list value"
	ErrInvalidAddrMsg       = "Invalid address value"

	ErrNoRegisteredAddress = "There are no address "
)
