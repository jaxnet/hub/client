package utils

import (
	"bufio"
	"client/core/actions/context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/types/btcjson"
	clientindexer "gitlab.com/jaxnet/shard.indexer/client"
	"io/ioutil"
	"os/exec"
	"strings"
	"testing"
	"time"
)

type MessageAndFlag struct {
	Message string
	Flag    bool
}

type MessageForCheck struct {
	Message           string
	ExpectedInclusion bool
	ActualInclusion   bool
}

type BalanceForCheck struct {
	ShardID         uint32
	Address         string
	ExpectedBalance int64
}

func ProcessCommand(t *testing.T, cmd *exec.Cmd, messages ...*MessageForCheck) {
	stdout, err := cmd.StderrPipe()
	if err != nil {
		panic(t)
	}
	err = cmd.Start()
	if err != nil {
		panic(t)
	}
	reader := bufio.NewScanner(stdout)
	for reader.Scan() {
		for _, message := range messages {
			if strings.Contains(reader.Text(), message.Message) {
				message.ActualInclusion = true
			}
		}
	}
	err = stdout.Close()
	return
}

func ResetTestingEnvironment(t *testing.T) {
	cmdResetEnv := exec.Command(
		"ansible-playbook",
		"-i",
		"inventory.cfg",
		"deploy-env.yml",
		"--limit",
		ResetEnvUser)
	cmdResetEnv.Dir = ResetEnvCmdDir
	err := cmdResetEnv.Start()
	if err != nil {
		panic(t)
	}
	err = cmdResetEnv.Wait()
	if err != nil {
		panic(t)
	}

	// for getting enough time to start environment
	time.Sleep(time.Second * 25)
}

func NewClientIndexer(t *testing.T) (clientIndexer clientindexer.Indexer) {
	err := loadSettings()
	if err != nil {
		panic(t)
	}

	clientIndexer, err = clientindexer.NewIndexer(&clientindexer.Config{
		Rpc: clientindexer.ConnConfig{
			Host: fmt.Sprintf("http://%s/api/v1", Conf.Indexer.Net.Interface()),
		},
	})
	if err != nil {
		panic(t)
	}
	return
}

func GetBalance(t *testing.T, clientIndexer clientindexer.Indexer, shardID uint32, address string) int64 {
	balance, err := clientIndexer.Balance(shardID, address)
	if err != nil {
		panic(t)
	}
	return balance
}

func GetRegistrationEADAddresses() (listEADAddresses *btcjson.ListEADAddresses, err error) {
	txMan, err := newTxMan()
	if err != nil {
		return
	}

	_, senderPubKey, _, err := loadCredentials()
	if err != nil {
		return
	}

	clientPubKeyAddress, err := btcutil.NewAddressPubKey(senderPubKey.SerializeUncompressed(), Conf.Network)
	if err != nil {
		return
	}
	clientPubKeyAddressStr := clientPubKeyAddress.String()

	listEADAddresses, err = txMan.RPC().ListEADAddresses(nil, &clientPubKeyAddressStr)
	return
}

func ConvertRegistrationAddressesToCmdArgs(eadAddresses []context.EADAddress) (cmdArgs []string) {
	for _, eadAddress := range eadAddresses {
		cmdArgs = append(cmdArgs, CmdArgAddrAndShardsFlag)
		cmdArgs = append(cmdArgs, fmt.Sprintf("%s:%d:%d:%s",
			eadAddress.IP.String(), eadAddress.Port, eadAddress.ExpDate,
			strings.Trim(strings.Join(strings.Fields(fmt.Sprint(eadAddress.Shards)), ","), "[]")))
	}
	return
}

func ConvertRegistrationAddressesForDelToCmdArgs(eadAddresses []context.EADAddress) (cmdArgs []string) {
	for _, eadAddress := range eadAddresses {
		cmdArgs = append(cmdArgs, CmdArgAddrAndShardsFlag)
		cmdArg := fmt.Sprintf("%s:%d", eadAddress.IP.String(), eadAddress.Port)
		if len(eadAddress.Shards) != 0 {
			cmdArg = cmdArg + ":" + strings.Trim(strings.Join(strings.Fields(fmt.Sprint(eadAddress.Shards)), ","), "[]")
		}
		cmdArgs = append(cmdArgs, cmdArg)
	}
	return
}

func ConvertRegistrationAddressesStrToCmdArgs(eadAddresses []string) (cmdArgs []string) {
	for _, eadAddress := range eadAddresses {
		cmdArgs = append(cmdArgs, CmdArgAddrAndShardsFlag)
		cmdArgs = append(cmdArgs, eadAddress)
	}
	return
}

func CmpExpectedRegisteredAddressesWithActual(
	t *testing.T, expectedEADAddresses []context.EADAddress) {
	actualEADAddresses, err := GetRegistrationEADAddresses()
	if err != nil {
		panic(t)
	}
	if len(expectedEADAddresses) == 0 {
		assert.Equal(t, 0, len(actualEADAddresses.Agents))
		return
	}
	actualEADAgent, ok := actualEADAddresses.Agents[EADAddressPubKey]
	assert.Equal(t, true, ok)
	assert.Equal(t, len(expectedEADAddresses), len(actualEADAgent.IPs))
	for _, expEADAddress := range expectedEADAddresses {
		ok, actualShardIDs := getShardsFromAgentByIPAndPort(
			expEADAddress.IP.String(), uint16(expEADAddress.Port), actualEADAgent.IPs)
		assert.Equal(t, true, ok)
		assert.Equal(t, expEADAddress.Shards, actualShardIDs)
	}
}

func GetNetworkFee(t *testing.T, shardID uint32) (netFee int64) {
	txMan, err := newTxMan()
	if err != nil {
		panic(t)
	}

	netFee, err = txMan.ForShard(shardID).NetworkFee()
	if err != nil {
		panic(t)
	}
	return
}

func loadCredentials() (
	pKeyHex string, pubKey *btcec.PublicKey, address *btcutil.AddressPubKeyHash, err error) {

	pKeyHexBinary, err := ioutil.ReadFile("pkey.key")
	if err != nil {
		return
	}

	pKeyHex = string(pKeyHexBinary)
	k, err := txutils.NewKeyData(pKeyHex, Conf.Network)
	if err != nil {
		return
	}

	pKey := k.PrivateKey
	pubKey = pKey.PubKey()
	address = k.AddressPubKey.AddressPubKeyHash()
	return
}

func newTxMan() (txMan *txutils.TxMan, err error) {
	err = loadSettings()
	if err != nil {
		return
	}

	senderPKeyHex, _, _, err := loadCredentials()
	if err != nil {
		return
	}

	gatlingRPCParams := txutils.ManagerCfg{
		Net: Conf.Blockchain.NetworkName,
		RPC: txutils.NodeRPC{
			Host: Conf.Blockchain.RPC.Net.Interface(),
			User: Conf.Blockchain.RPC.Credentials.User,
			Pass: Conf.Blockchain.RPC.Credentials.Pass,
		},

		PrivateKey: senderPKeyHex,
	}

	txMan, err = txutils.NewTxMan(gatlingRPCParams)
	return
}

func getShardsFromAgentByIPAndPort(
	ip string, port uint16, actualEADAddresses []btcjson.EADAddress) (ok bool, shardIDs []uint32) {
	for _, eadAddress := range actualEADAddresses {
		if eadAddress.IP == ip && eadAddress.Port == port {
			ok = true
			shardIDs = eadAddress.Shards
			return
		}
	}
	ok = false
	return
}
