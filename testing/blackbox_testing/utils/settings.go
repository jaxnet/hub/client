package utils

import (
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

var (
	Conf *Settings
)

type CredentialsConfig struct {
	User string `yaml:"user"`
	Pass string `yaml:"pass"`
}

type NetworkConfig struct {
	Address string `yaml:"address"`
	Port    uint16 `yaml:"port"`
}

func (c NetworkConfig) Interface() string {
	return fmt.Sprint(c.Address, ":", c.Port)
}

type RPCConfig struct {
	Net         NetworkConfig
	Credentials CredentialsConfig
}

type BlockchainConfig struct {
	NetworkName string    `yaml:"network-name"`
	RPC         RPCConfig `yaml:"rpc"`
}

type Settings struct {
	Blockchain BlockchainConfig `yaml:"blockchain"`
	Network    *chaincfg.Params
	Indexer    RPCConfig `yaml:"indexer"`
}

func loadSettings() (err error) {
	Conf = &Settings{}

	data, err := ioutil.ReadFile("conf.yaml")
	if err != nil {
		return
	}

	err = yaml.Unmarshal(data, Conf)
	if err != nil {
		return
	}

	if Conf.Blockchain.NetworkName == "fastnet" {
		Conf.Network = &chaincfg.FTestNetParams
	}
	return
}
