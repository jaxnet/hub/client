package transactions

import (
	"client/testing/blackbox_testing/utils"
	"fmt"
	"testing"
)

func TestRegularTxNormalFlow(t *testing.T) {
	utils.ResetTestingEnvironment(t)
	senderAddress := "minPGY4BWGhNoSofaVow4GvPpMX5bmMTbR"
	receiverAddress := "muJAuq1YiEhEvgsZBS6enE1QvVTCLvKF2w"
	var shardID uint32 = 1
	var txAmount int64 = 100000000
	networkFee := utils.GetNetworkFee(t, shardID)
	var senderInitialBalance int64 = 30000000000
	//var senderFinalBalance int64 = 29899999000
	senderFinalBalance := senderInitialBalance - txAmount - networkFee
	var receiverInitialBalance int64 = 0
	receiverFinalBalance := receiverInitialBalance + txAmount

	commandArgs := []string{
		utils.CmdArgSend,
		utils.CmdArgShardIDFlag,
		fmt.Sprintf("%d", shardID),
		utils.CmdArgAmountFlag,
		fmt.Sprintf("%d", txAmount),
		utils.CmdArgDestAddrFlag,
		receiverAddress}

	regularTxTestBuilder := NewRegularTxTestBuilder(t).
		SetCmdCommandName(utils.ClientExecPath).
		SetCmdCommandArgs(commandArgs...).
		SetMessageForCheck(utils.TxConfirmedMsg, true).
		SetBalanceForCheck(shardID, senderAddress, senderFinalBalance).
		SetBalanceForCheck(shardID, receiverAddress, receiverFinalBalance)
	regularTxTestBuilder.RunTest()
}

func TestRegularTxNormalFlowTwoTransactions(t *testing.T) {
	utils.ResetTestingEnvironment(t)
	senderAddress := "minPGY4BWGhNoSofaVow4GvPpMX5bmMTbR"
	receiverAddress := "muJAuq1YiEhEvgsZBS6enE1QvVTCLvKF2w"
	var shardID uint32 = 1
	var txAmount int64 = 100000000
	networkFee := utils.GetNetworkFee(t, shardID)
	var senderInitialBalance int64 = 30000000000
	senderFinalBalance := senderInitialBalance - 2*(txAmount+networkFee)
	var receiverInitialBalance int64 = 0
	receiverFinalBalance := receiverInitialBalance + 2*txAmount

	commandArgs := []string{
		utils.CmdArgSend,
		utils.CmdArgShardIDFlag,
		fmt.Sprintf("%d", shardID),
		utils.CmdArgAmountFlag,
		fmt.Sprintf("%d", txAmount),
		utils.CmdArgDestAddrFlag,
		receiverAddress}

	regularTxTestBuilder := NewRegularTxTestBuilder(t).
		SetCmdCommandName(utils.ClientExecPath).
		SetCmdCommandArgs(commandArgs...).
		SetMessageForCheck(utils.TxConfirmedMsg, true)
	regularTxTestBuilder.RunTest()

	regularTxTestBuilder = regularTxTestBuilder.SetBalanceForCheck(shardID, senderAddress, senderFinalBalance).
		SetBalanceForCheck(shardID, receiverAddress, receiverFinalBalance)
	regularTxTestBuilder.RunTest()
}

func TestRegularTxNormalFlowThreeTransactions(t *testing.T) {
	utils.ResetTestingEnvironment(t)
	senderAddress := "minPGY4BWGhNoSofaVow4GvPpMX5bmMTbR"
	receiverAddress := "muJAuq1YiEhEvgsZBS6enE1QvVTCLvKF2w"
	var shardID uint32 = 1
	var txAmount int64 = 100000000
	networkFee := utils.GetNetworkFee(t, shardID)
	var senderInitialBalance int64 = 30000000000
	senderFinalBalance := senderInitialBalance - 3*(txAmount+networkFee)
	var receiverInitialBalance int64 = 0
	receiverFinalBalance := receiverInitialBalance + 3*txAmount

	commandArgs := []string{
		utils.CmdArgSend,
		utils.CmdArgShardIDFlag,
		fmt.Sprintf("%d", shardID),
		utils.CmdArgAmountFlag,
		fmt.Sprintf("%d", txAmount),
		utils.CmdArgDestAddrFlag,
		receiverAddress}

	regularTxTestBuilder := NewRegularTxTestBuilder(t).
		SetCmdCommandName(utils.ClientExecPath).
		SetCmdCommandArgs(commandArgs...).
		SetMessageForCheck(utils.TxConfirmedMsg, true)
	regularTxTestBuilder.RunTest()

	regularTxTestBuilder.RunTest()

	regularTxTestBuilder = regularTxTestBuilder.SetBalanceForCheck(shardID, senderAddress, senderFinalBalance).
		SetBalanceForCheck(shardID, receiverAddress, receiverFinalBalance)
	regularTxTestBuilder.RunTest()
}

func TestRegularTxInvalidArg(t *testing.T) {
	tests := []struct {
		name        string
		shardID     string
		amount      string
		destAddress string
		errMsg      string
	}{
		{
			name:        "TestInvalidDestinationAddress",
			shardID:     utils.CmdArgShardIDValCorrect,
			amount:      utils.CmdArgAmountValCorrect,
			destAddress: utils.CmdArgDestAddrValInvalid,
			errMsg:      utils.ErrDestAddrMsg,
		},
		{
			name:        "TestInvalidShardID",
			shardID:     utils.CmdArgShardIDValInvalid,
			amount:      utils.CmdArgAmountValCorrect,
			destAddress: utils.CmdArgDestAddrValCorrect,
			errMsg:      utils.ErrShardIDInvalidMsg,
		},
		{
			name:        "TestInvalidShardIDNegativeVal",
			shardID:     utils.CmdArgShardIDValNegative,
			amount:      utils.CmdArgAmountValCorrect,
			destAddress: utils.CmdArgDestAddrValCorrect,
			errMsg:      utils.ErrShardIDInvalidMsg,
		},
		{
			name:        "TestInvalidShardIDOverflowVal",
			shardID:     utils.CmdArgShardIDValOverflow,
			amount:      utils.CmdArgAmountValCorrect,
			destAddress: utils.CmdArgDestAddrValCorrect,
			errMsg:      utils.ErrShardIDInvalidMsg,
		},
		{
			name:        "TestUnsupportedShardID",
			shardID:     utils.CmdArgShardIDValUnsupported,
			amount:      utils.CmdArgAmountValCorrect,
			destAddress: utils.CmdArgDestAddrValCorrect,
			errMsg:      utils.ErrShardIDNotMatchMsg,
		},
		{
			name:        "TestInvalidAmountInvalidVal",
			shardID:     utils.CmdArgShardIDValCorrect,
			amount:      utils.CmdArgAmountValInvalid,
			destAddress: utils.CmdArgDestAddrValCorrect,
			errMsg:      utils.ErrInvalidAmountMsg,
		},
		{
			name:        "TestInvalidAmountNegativeVal",
			shardID:     utils.CmdArgShardIDValCorrect,
			amount:      utils.CmdArgAmountValNegative,
			destAddress: utils.CmdArgDestAddrValCorrect,
			errMsg:      utils.ErrInvalidAmountMsg,
		},
		{
			name:        "TestInvalidAmountOverflowVal",
			shardID:     utils.CmdArgShardIDValCorrect,
			amount:      utils.CmdArgAmountValOverflow,
			destAddress: utils.CmdArgDestAddrValCorrect,
			errMsg:      utils.ErrInvalidAmountMsg,
		},
		{
			name:        "TestInvalidAmountNotEnoughFunds",
			shardID:     utils.CmdArgShardIDValCorrect,
			amount:      utils.CmdArgAmountValTooBig,
			destAddress: utils.CmdArgDestAddrValCorrect,
			errMsg:      utils.ErrNotEnoughAmountMsg,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			utils.ResetTestingEnvironment(t)
			senderAddress := "minPGY4BWGhNoSofaVow4GvPpMX5bmMTbR"
			receiverAddress := "muJAuq1YiEhEvgsZBS6enE1QvVTCLvKF2w"
			var shardID uint32 = 1
			var senderFinalBalance int64 = 30000000000
			var receiverFinalBalance int64 = 0

			commandArgs := []string{
				utils.CmdArgSend,
				utils.CmdArgShardIDFlag,
				tt.shardID,
				utils.CmdArgAmountFlag,
				tt.amount,
				utils.CmdArgDestAddrFlag,
				tt.destAddress,
			}

			regularTxTestBuilder := NewRegularTxTestBuilder(t).
				SetCmdCommandName(utils.ClientExecPath).
				SetCmdCommandArgs(commandArgs...).
				SetMessageForCheck(utils.TxFundsSendingMsg, false).
				SetMessageForCheck(tt.errMsg, true).
				SetBalanceForCheck(shardID, senderAddress, senderFinalBalance).
				SetBalanceForCheck(shardID, receiverAddress, receiverFinalBalance)

			regularTxTestBuilder.RunTest()
		})
	}
}
