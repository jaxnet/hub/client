package transactions

import (
	"client/testing/blackbox_testing/utils"
	"github.com/stretchr/testify/assert"
	"os/exec"
	"testing"
)

type RegularTxTestBuilder interface {
	SetCmdCommandName(cmdCommandName string) RegularTxTestBuilder
	SetCmdCommandArgs(cmdCommandArgs ...string) RegularTxTestBuilder
	SetBalanceForCheck(shardID uint32, address string, expectedBalance int64) RegularTxTestBuilder
	SetMessageForCheck(message string, expectedInclusion bool) RegularTxTestBuilder

	RunTest()
}

type regularTxTestBuilder struct {
	err error
	t   *testing.T

	cmdCommandName string
	cmdCommandArgs []string

	balancesForCheck []utils.BalanceForCheck
	messagesForCheck []*utils.MessageForCheck
}

func NewRegularTxTestBuilder(t *testing.T) RegularTxTestBuilder {
	return &regularTxTestBuilder{
		t: t,

		cmdCommandName: "",
		cmdCommandArgs: []string{},

		balancesForCheck: []utils.BalanceForCheck{},
		messagesForCheck: []*utils.MessageForCheck{},
	}
}

func (rtb *regularTxTestBuilder) SetCmdCommandName(cmdCommandName string) RegularTxTestBuilder {
	rtb.cmdCommandName = cmdCommandName
	return rtb
}

func (rtb *regularTxTestBuilder) SetCmdCommandArgs(cmdCommandArgs ...string) RegularTxTestBuilder {
	rtb.cmdCommandArgs = cmdCommandArgs
	return rtb
}

func (rtb *regularTxTestBuilder) SetBalanceForCheck(
	shardID uint32, address string, expectedBalance int64) RegularTxTestBuilder {
	rtb.balancesForCheck = append(rtb.balancesForCheck,
		utils.BalanceForCheck{
			ShardID:         shardID,
			Address:         address,
			ExpectedBalance: expectedBalance,
		})
	return rtb
}

func (rtb *regularTxTestBuilder) SetMessageForCheck(message string, expectedInclusion bool) RegularTxTestBuilder {
	rtb.messagesForCheck = append(rtb.messagesForCheck,
		&utils.MessageForCheck{
			Message:           message,
			ExpectedInclusion: expectedInclusion,
			ActualInclusion:   false,
		})
	return rtb
}

func (rtb *regularTxTestBuilder) RunTest() {
	cmd := exec.Command(rtb.cmdCommandName, rtb.cmdCommandArgs...)
	utils.ProcessCommand(rtb.t, cmd, rtb.messagesForCheck...)
	for _, messageForCheck := range rtb.messagesForCheck {
		assert.Equal(rtb.t, messageForCheck.ExpectedInclusion, messageForCheck.ActualInclusion)
	}

	if len(rtb.balancesForCheck) == 0 {
		return
	}

	clientIndexer := utils.NewClientIndexer(rtb.t)
	for _, balanceForCheck := range rtb.balancesForCheck {
		actualBalance := utils.GetBalance(rtb.t, clientIndexer, balanceForCheck.ShardID, balanceForCheck.Address)
		assert.Equal(rtb.t, balanceForCheck.ExpectedBalance, actualBalance)
	}
}
