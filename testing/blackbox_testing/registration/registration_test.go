package registration

import (
	"client/core/actions/context"
	"client/testing/blackbox_testing/utils"
	"net"
	"testing"
)

func TestRegistrationNormalFlow(t *testing.T) {
	fillAddress := func(ip string, port int64, expDate int64, shards []uint32) context.EADAddress {
		return context.EADAddress{
			IP:      net.ParseIP(ip),
			Port:    port,
			ExpDate: expDate,
			Shards:  shards,
		}
	}

	tests := []struct {
		name      string
		addresses []context.EADAddress
	}{
		{
			name: "TestRegistrationOneIPv4AddressOneShardNormalFlow",
			addresses: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1}),
			},
		},
		{
			name: "TestRegistrationOneIPv4AddrSeveralShardsNormalFlow",
			addresses: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2}),
			},
		},
		{
			name: "TestRegistrationTwoIPv4AddrSeveralShardsNormalFlow",
			addresses: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2}),
				fillAddress("35.117.202.15", 3333, 365, []uint32{1, 2}),
			},
		},
		{
			name: "TestRegistrationOneIPv6AddressOneShardNormalFlow",
			addresses: []context.EADAddress{
				fillAddress("2001:0db8:85a3:0023:4367:8a2e:0370:7334", 8888, 30, []uint32{1}),
			},
		},
		{
			name: "TestRegistrationOneIPv6AddrSeveralShardsNormalFlow",
			addresses: []context.EADAddress{
				fillAddress("2001:0db8:85a3:0023:4367:8a2e:0370:7334", 8888, 30, []uint32{1, 2}),
			},
		},
		{
			name: "TestRegistrationTwoIPv6AddrSeveralShardsNormalFlow",
			addresses: []context.EADAddress{
				fillAddress("2001:0db8:85a3:0023:4367:8a2e:0370:7334", 8888, 30, []uint32{1, 2}),
				fillAddress("2002:0db8:85a3:0023:4367:8a2e:0370:7334", 3333, 365, []uint32{1, 2}),
			},
		},
		{
			name: "TestRegistrationIPv4AndIPv6AddrSeveralShardsNormalFlow",
			addresses: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2}),
				fillAddress("2002:0db8:85a3:0023:4367:8a2e:0370:7334", 3333, 365, []uint32{1, 2}),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			utils.ResetTestingEnvironment(t)
			registrationTxTestBuilder := NewRegTxTestBuilder(t).
				SetCmdCommandName(utils.ClientExecPath).
				SetEADAddressForReg(tt.addresses...).
				SetIsCheckRegisteredAndUpdatedAddresses().
				SetMessageForRegCheck(utils.TxRegistrationCreateMsg, true).
				SetMessageForRegCheck(utils.TxRegistrationSentMsg, true).
				SetMessageForRegCheck(utils.TxConfirmedMsg, true)

			registrationTxTestBuilder.RunTest()
		})
	}
}

func TestRegistrationInvalidArgs(t *testing.T) {
	tests := []struct {
		name      string
		addresses []string
		errMsg    string
	}{
		{
			name:      "TestRegistrationInvalidParameterMissIPv4Part",
			addresses: []string{"8888:30:1"},
			errMsg:    utils.ErrInvalidAddrMsg,
		},
		{
			name:      "TestRegistrationInvalidParameterMissIPv6Part",
			addresses: []string{"0db8:85a3:0023:4367:8a2e:0370:7334:8888:30:1,2"},
			errMsg:    utils.ErrInvalidAddrMsg,
		},
		{
			name:      "TestRegistrationIPv4InvalidParameterMissPort",
			addresses: []string{"34.117.202.15:30:1,2"},
			errMsg:    utils.ErrInvalidAddrMsg,
		},
		{
			name:      "TestRegistrationIPv6InvalidParameterMissPort",
			addresses: []string{"2002:0db8:85a3:0023:4367:8a2e:0370:7334:30:1,2"},
			errMsg:    utils.ErrInvalidAddrMsg,
		},
		{
			name:      "TestRegistrationIPv4InvalidParameterMissExpTime",
			addresses: []string{"34.117.202.15:8888:1"},
			errMsg:    utils.ErrInvalidAddrMsg,
		},
		{
			name:      "TestRegistrationIPv6InvalidParameterMissExpTime",
			addresses: []string{"2002:0db8:85a3:0023:4367:8a2e:0370:7334:8888:1,2"},
			errMsg:    utils.ErrInvalidAddrMsg,
		},
		{
			name:      "TestRegistrationIPv4InvalidParameterMissShards",
			addresses: []string{"34.117.202.15:8888:30"},
			errMsg:    utils.ErrInvalidAddrMsg,
		},
		{
			name:      "TestRegistrationIPv6InvalidParameterMissShards",
			addresses: []string{"2002:0db8:85a3:0023:4367:8a2e:0370:7334:8888:30"},
			errMsg:    utils.ErrInvalidAddrMsg,
		},
		{
			name:      "TestRegistrationIPv4InvalidIPAddress",
			addresses: []string{"34.600.202.15:8888:30:1"},
			errMsg:    utils.ErrInvalidIPv4ValueMsg,
		},
		{
			name:      "TestRegistrationIPv4InvalidPort",
			addresses: []string{"34.117.202.15:ddd:30:1"},
			errMsg:    utils.ErrInvalidPortValueMsg,
		},
		{
			name:      "TestRegistrationIPv4InvalidExpTime",
			addresses: []string{"34.117.202.15:8888:aa:1"},
			errMsg:    utils.ErrInvalidExpTimeMsg,
		},
		{
			name:      "TestRegistrationIPv4InvalidShardID",
			addresses: []string{"34.117.202.15:8888:30:1,a"},
			errMsg:    utils.ErrInvalidShardsListMsg,
		},
		{
			name:      "TestRegistrationIPv6InvalidIPAddress",
			addresses: []string{"2002:tdb8:85a3:0023:4367:8a2e:0370:7334:8888:30:1"},
			errMsg:    utils.ErrInvalidIPv6ValueMsg,
		},
		{
			name:      "TestRegistrationIPv6InvalidPort",
			addresses: []string{"2002:0db8:85a3:0023:4367:8a2e:0370:7334:ddd:30:1"},
			errMsg:    utils.ErrInvalidPortValueMsg,
		},
		{
			name:      "TestRegistrationIPv6InvalidExpTime",
			addresses: []string{"2002:0db8:85a3:0023:4367:8a2e:0370:7334:8888:aa:1"},
			errMsg:    utils.ErrInvalidExpTimeMsg,
		},
		{
			name:      "TestRegistrationIPv6InvalidShardID",
			addresses: []string{"2002:0db8:85a3:0023:4367:8a2e:0370:7334:8888:30:1,a"},
			errMsg:    utils.ErrInvalidShardsListMsg,
		},
		//{
		//	name: "TestRegistrationIPv6UnsupportedShardID",
		//	addresses: []string{"2002:0db8:85a3:0023:4367:8a2e:0370:7334:8888:30:1,1000"},
		//	errMsg: utils.ErrInvalidShardsListMsg,
		//},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			utils.ResetTestingEnvironment(t)
			registrationTxTestBuilder := NewRegTxTestBuilder(t).
				SetCmdCommandName(utils.ClientExecPath).
				SetEADWrongAddressForReg(tt.addresses...).
				SetMessageForRegCheck(utils.TxRegistrationCreateMsg, false).
				SetMessageForRegCheck(utils.TxRegistrationSentMsg, false).
				SetMessageForRegCheck(tt.errMsg, true).
				SetDelayBeforeChecking(15)

			registrationTxTestBuilder.RunTest()
		})
	}
}

func TestRegistrationUpdate(t *testing.T) {
	fillAddress := func(ip string, port int64, expDate int64, shards []uint32) context.EADAddress {
		return context.EADAddress{
			IP:      net.ParseIP(ip),
			Port:    port,
			ExpDate: expDate,
			Shards:  shards,
		}
	}

	tests := []struct {
		name           string
		addressesReg   []context.EADAddress
		addressesUpd   []context.EADAddress
		addressesCheck []context.EADAddress
	}{
		{
			name: "TestRegistrationUpdateAddNewAddress",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2}),
			},
			addressesUpd: []context.EADAddress{
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 7777, 60, []uint32{3, 4}),
			},
		},
		{
			name: "TestRegistrationUpdateAddNewShardToExistingAddress",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2}),
			},
			addressesUpd: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{3, 4}),
			},
			addressesCheck: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3, 4}),
			},
		},
		{
			name: "TestRegistrationUpdateAddExistingShardToExistingAddress",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2}),
			},
			addressesUpd: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{2}),
			},
			addressesCheck: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2}),
			},
		},
		{
			name: "TestRegistrationUpdateAddExistingAndNewShardToExistingAddress",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2}),
			},
			addressesUpd: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{2, 3}),
			},
			addressesCheck: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
			},
		},
		{
			name: "TestRegistrationUpdateAddTheSameIPAndDifferPort",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2}),
			},
			addressesUpd: []context.EADAddress{
				fillAddress("34.117.202.15", 8887, 30, []uint32{1, 2}),
			},
			addressesCheck: []context.EADAddress{
				fillAddress("34.117.202.15", 8887, 30, []uint32{1, 2}),
			},
		},
		{
			name: "TestRegistrationUpdateAddTheSameIPAndDifferPortAndNewShard",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2}),
			},
			addressesUpd: []context.EADAddress{
				fillAddress("34.117.202.15", 8887, 30, []uint32{3}),
			},
			addressesCheck: []context.EADAddress{
				fillAddress("34.117.202.15", 8887, 30, []uint32{1, 2, 3}),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			utils.ResetTestingEnvironment(t)
			registrationTxTestBuilder := NewRegTxTestBuilder(t).
				SetCmdCommandName(utils.ClientExecPath).
				SetEADAddressForReg(tt.addressesReg...).
				SetEADAddressToUpd(tt.addressesUpd...).
				SetEADAddressToCheck(tt.addressesCheck...).
				SetMessageForRegCheck(utils.TxRegistrationCreateMsg, true).
				SetMessageForRegCheck(utils.TxRegistrationSentMsg, true).
				SetMessageForUpdCheck(utils.TxRegistrationCreateMsg, true).
				SetMessageForUpdCheck(utils.TxRegistrationSentMsg, true)

			if len(tt.addressesCheck) == 0 {
				registrationTxTestBuilder = registrationTxTestBuilder.SetIsCheckRegisteredAndUpdatedAddresses()
			}

			registrationTxTestBuilder.RunTest()
		})
	}
}

func TestRegistrationDeleteNormalFlow(t *testing.T) {
	fillAddress := func(ip string, port int64, expDate int64, shards []uint32) context.EADAddress {
		return context.EADAddress{
			IP:      net.ParseIP(ip),
			Port:    port,
			ExpDate: expDate,
			Shards:  shards,
		}
	}

	tests := []struct {
		name           string
		addressesReg   []context.EADAddress
		addressesDel   []context.EADAddress
		addressesCheck []context.EADAddress
	}{
		{
			name: "TestRegistrationDeleteOneMiddleShard",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
			},
			addressesDel: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 0, []uint32{2}),
			},
			addressesCheck: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 0, []uint32{1, 3}),
			},
		},
		{
			name: "TestRegistrationDeleteOneFirstShard",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
			},
			addressesDel: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 0, []uint32{1}),
			},
			addressesCheck: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 0, []uint32{2, 3}),
			},
		},
		{
			name: "TestRegistrationDeleteOneLastShard",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
			},
			addressesDel: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 0, []uint32{3}),
			},
			addressesCheck: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 0, []uint32{1, 2}),
			},
		},
		{
			name: "TestRegistrationDeleteAllShardsForOneAddress",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 30, []uint32{1, 2, 3}),
			},
			addressesDel: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 0, []uint32{1, 2, 3}),
			},
			addressesCheck: []context.EADAddress{
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 0, []uint32{1, 2, 3}),
			},
		},
		{
			name: "TestRegistrationDeleteWholeOneAddress",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 30, []uint32{1, 2, 3}),
			},
			addressesDel: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 0, []uint32{}),
			},
			addressesCheck: []context.EADAddress{
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 30, []uint32{1, 2, 3}),
			},
		},
		{
			name: "TestRegistrationDeleteOneShardInEachAddress",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 30, []uint32{1, 2, 3}),
			},
			addressesDel: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 0, []uint32{2}),
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 0, []uint32{2}),
			},
			addressesCheck: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 0, []uint32{1, 3}),
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 0, []uint32{1, 3}),
			},
		},
		{
			name: "TestRegistrationDeleteWholeShardsInWholeAddresses",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 30, []uint32{1, 2, 3}),
			},
			addressesDel: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 0, []uint32{1, 2, 3}),
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 0, []uint32{1, 2, 3}),
			},
		},
		{
			name: "TestRegistrationDeleteWholeAddresses",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 30, []uint32{1, 2, 3}),
			},
			addressesDel: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 0, []uint32{}),
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 0, []uint32{}),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			utils.ResetTestingEnvironment(t)
			registrationTxTestBuilder := NewRegTxTestBuilder(t).
				SetCmdCommandName(utils.ClientExecPath).
				SetEADAddressForReg(tt.addressesReg...).
				SetEADAddressToDel(tt.addressesDel...).
				SetEADAddressToCheck(tt.addressesCheck...).
				SetMessageForRegCheck(utils.TxRegistrationCreateMsg, true).
				SetMessageForRegCheck(utils.TxRegistrationSentMsg, true).
				SetMessageForDelCheck(utils.TxRegistrationCreateMsg, true).
				SetMessageForDelCheck(utils.TxRegistrationSentMsg, true)

			registrationTxTestBuilder.RunTest()
		})
	}
}

func TestRegistrationDeleteNotRegisteredData(t *testing.T) {
	fillAddress := func(ip string, port int64, expDate int64, shards []uint32) context.EADAddress {
		return context.EADAddress{
			IP:      net.ParseIP(ip),
			Port:    port,
			ExpDate: expDate,
			Shards:  shards,
		}
	}

	tests := []struct {
		name         string
		addressesReg []context.EADAddress
		addressesDel []context.EADAddress
		errMsg       string
	}{
		{
			name: "TestRegistrationDeleteNotRegisteredIPv4Address",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 30, []uint32{1, 2, 3}),
			},
			addressesDel: []context.EADAddress{
				fillAddress("34.117.202.0", 8888, 0, []uint32{}),
			},
			errMsg: utils.ErrInvalidIPv4ValueMsg,
		},
		{
			name: "TestRegistrationDeleteNotRegisteredIPv6Address",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:7334", 8888, 30, []uint32{1, 2, 3}),
			},
			addressesDel: []context.EADAddress{
				fillAddress("2002:0db8:85a3:0000:0001:8a2e:0370:0730", 8888, 0, []uint32{}),
			},
			errMsg: utils.ErrInvalidIPv6ValueMsg,
		},
		{
			name: "TestRegistrationDeleteNotRegisteredPort",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
			},
			addressesDel: []context.EADAddress{
				fillAddress("34.117.202.15", 8887, 0, []uint32{}),
			},
			errMsg: utils.ErrInvalidPortValueMsg,
		},
		{
			name: "TestRegistrationDeleteNotRegisteredShard",
			addressesReg: []context.EADAddress{
				fillAddress("34.117.202.15", 8888, 30, []uint32{1, 2, 3}),
			},
			addressesDel: []context.EADAddress{
				fillAddress("34.117.202.15", 8887, 0, []uint32{2, 5}),
			},
			errMsg: utils.ErrInvalidShardsListMsg,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			utils.ResetTestingEnvironment(t)
			registrationTxTestBuilder := NewRegTxTestBuilder(t).
				SetCmdCommandName(utils.ClientExecPath).
				SetEADAddressForReg(tt.addressesReg...).
				SetEADAddressToDel(tt.addressesDel...).
				SetIsCheckRegisteredAndUpdatedAddresses().
				SetDelayBeforeChecking(15).
				SetMessageForRegCheck(utils.TxRegistrationCreateMsg, true).
				SetMessageForRegCheck(utils.TxRegistrationSentMsg, true).
				SetMessageForDelCheck(utils.TxRegistrationCreateMsg, false).
				SetMessageForDelCheck(utils.TxRegistrationSentMsg, false).
				SetMessageForDelCheck(utils.ErrNoRegisteredAddress, true)

			registrationTxTestBuilder.RunTest()
		})
	}
}

func TestRegistrationDeleteInvalidArgs(t *testing.T) {
	tests := []struct {
		name         string
		addressesDel []string
		errMsg       string
	}{
		{
			name:         "TestRegistrationDeleteInvalidParameterMissIPv4Part",
			addressesDel: []string{"8888:1,2"},
			errMsg:       utils.ErrInvalidAddrMsg,
		},
		{
			name:         "TestRegistrationDeleteInvalidParameterMissIPv6Part",
			addressesDel: []string{"0db8:85a3:0000:0001:8a2e:0370:7334:8888:1,2"},
			errMsg:       utils.ErrInvalidAddrMsg,
		},
		{
			name:         "TestRegistrationDeleteIPv4InvalidParameterMissPort",
			addressesDel: []string{"34.117.202.15:1,2"},
			errMsg:       utils.ErrInvalidAddrMsg,
		},
		{
			name:         "TestRegistrationDeleteIPv6InvalidParameterMissPort",
			addressesDel: []string{"2002:0db8:85a3:0000:0001:8a2e:0370:7334:1,2"},
			errMsg:       utils.ErrInvalidAddrMsg,
		},
		{
			name:         "TestRegistrationDeleteIPv4InvalidIPAddress",
			addressesDel: []string{"34.600.202.15:8888:1"},
			errMsg:       utils.ErrInvalidIPv4ValueMsg,
		},
		{
			name:         "TestRegistrationDeleteIPv4InvalidPort",
			addressesDel: []string{"34.117.202.15:ddd:1"},
			errMsg:       utils.ErrInvalidPortValueMsg,
		},
		{
			name:         "TestRegistrationDeleteIPv4InvalidShardID",
			addressesDel: []string{"34.117.202.15:8888:1,a"},
			errMsg:       utils.ErrInvalidShardsListMsg,
		},
		{
			name:         "TestRegistrationDeleteIPv6InvalidIPAddress",
			addressesDel: []string{"2002:tdb8:85a3:0000:0001:8a2e:0370:7334:8888:1,2"},
			errMsg:       utils.ErrInvalidIPv6ValueMsg,
		},
		{
			name:         "TestRegistrationDeleteIPv6InvalidPort",
			addressesDel: []string{"2002:0db8:85a3:0000:0001:8a2e:0370:7334:ddd:1,2"},
			errMsg:       utils.ErrInvalidPortValueMsg,
		},
		{
			name:         "TestRegistrationDeleteIPv6InvalidShardID",
			addressesDel: []string{"2002:0db8:85a3:0000:0001:8a2e:0370:7334:8888:1,a"},
			errMsg:       utils.ErrInvalidShardsListMsg,
		},
		{
			name:         "TestRegistrationDeleteIPv6UnsupportedShardID",
			addressesDel: []string{"2002:0db8:85a3:0000:0001:8a2e:0370:7334:8888:1,1000"},
			errMsg:       utils.ErrInvalidShardsListMsg,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			utils.ResetTestingEnvironment(t)
			registrationTxTestBuilder := NewRegTxTestBuilder(t).
				SetCmdCommandName(utils.ClientExecPath).
				SetEADWrongAddressToDel(tt.addressesDel...).
				SetMessageForRegCheck(utils.TxRegistrationCreateMsg, false).
				SetMessageForRegCheck(utils.TxRegistrationSentMsg, false).
				SetMessageForRegCheck(tt.errMsg, true).
				SetDelayBeforeChecking(15)

			registrationTxTestBuilder.RunTest()
		})
	}
}
