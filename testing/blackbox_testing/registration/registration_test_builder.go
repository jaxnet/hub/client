package registration

import (
	"client/core/actions/context"
	"client/testing/blackbox_testing/utils"
	"github.com/stretchr/testify/assert"
	"os/exec"
	"testing"
	"time"
)

type RegTxTestBuilder interface {
	SetCmdCommandName(cmdCommandName string) RegTxTestBuilder
	SetEADAddressForReg(eadAddress ...context.EADAddress) RegTxTestBuilder
	SetEADWrongAddressForReg(eadAddress ...string) RegTxTestBuilder
	SetEADAddressToUpd(eadAddress ...context.EADAddress) RegTxTestBuilder
	SetEADAddressToDel(eadAddress ...context.EADAddress) RegTxTestBuilder
	SetEADWrongAddressToDel(eadAddress ...string) RegTxTestBuilder
	SetEADAddressToCheck(eadAddress ...context.EADAddress) RegTxTestBuilder
	SetIsCheckRegisteredAndUpdatedAddresses() RegTxTestBuilder
	SetMessageForRegCheck(message string, expectedInclusion bool) RegTxTestBuilder
	SetMessageForUpdCheck(message string, expectedInclusion bool) RegTxTestBuilder
	SetMessageForDelCheck(message string, expectedInclusion bool) RegTxTestBuilder
	SetDelayBeforeChecking(delayInSec int) RegTxTestBuilder

	RunTest()
}

type regTxTestBuilder struct {
	err error
	t   *testing.T

	cmdCommandName       string
	eadAddressesToReg    []context.EADAddress
	eadWrongAddress      []string
	eadAddressesToUpd    []context.EADAddress
	eadAddressesToDel    []context.EADAddress
	eadWrongAddressToDel []string

	eadAddressesToCheck  []context.EADAddress
	isCheckRegAndUpdAddr bool

	messagesForRegCheck []*utils.MessageForCheck
	messagesForUpdCheck []*utils.MessageForCheck
	messagesForDelCheck []*utils.MessageForCheck

	DelayInSecBeforeChecking int
}

func NewRegTxTestBuilder(t *testing.T) RegTxTestBuilder {
	return &regTxTestBuilder{
		t: t,

		cmdCommandName:       "",
		eadAddressesToReg:    []context.EADAddress{},
		eadWrongAddress:      []string{},
		eadAddressesToUpd:    []context.EADAddress{},
		eadAddressesToDel:    []context.EADAddress{},
		eadWrongAddressToDel: []string{},

		eadAddressesToCheck:  []context.EADAddress{},
		isCheckRegAndUpdAddr: false,

		messagesForRegCheck: []*utils.MessageForCheck{},
		messagesForUpdCheck: []*utils.MessageForCheck{},
		messagesForDelCheck: []*utils.MessageForCheck{},

		DelayInSecBeforeChecking: 0,
	}
}

func (rtb *regTxTestBuilder) SetCmdCommandName(cmdCommandName string) RegTxTestBuilder {
	rtb.cmdCommandName = cmdCommandName
	return rtb
}

func (rtb *regTxTestBuilder) SetEADAddressForReg(eadAddress ...context.EADAddress) RegTxTestBuilder {
	rtb.eadAddressesToReg = append(rtb.eadAddressesToReg, eadAddress...)
	return rtb
}

func (rtb *regTxTestBuilder) SetEADWrongAddressForReg(eadAddress ...string) RegTxTestBuilder {
	rtb.eadWrongAddress = append(rtb.eadWrongAddress, eadAddress...)
	return rtb
}

func (rtb *regTxTestBuilder) SetEADAddressToUpd(eadAddress ...context.EADAddress) RegTxTestBuilder {
	rtb.eadAddressesToUpd = append(rtb.eadAddressesToUpd, eadAddress...)
	return rtb
}

func (rtb *regTxTestBuilder) SetEADAddressToDel(eadAddress ...context.EADAddress) RegTxTestBuilder {
	rtb.eadAddressesToDel = append(rtb.eadAddressesToDel, eadAddress...)
	return rtb
}

func (rtb *regTxTestBuilder) SetEADWrongAddressToDel(eadAddress ...string) RegTxTestBuilder {
	rtb.eadWrongAddressToDel = append(rtb.eadWrongAddressToDel, eadAddress...)
	return rtb
}

func (rtb *regTxTestBuilder) SetEADAddressToCheck(eadAddress ...context.EADAddress) RegTxTestBuilder {
	rtb.eadAddressesToCheck = append(rtb.eadAddressesToCheck, eadAddress...)
	return rtb
}

func (rtb *regTxTestBuilder) SetIsCheckRegisteredAndUpdatedAddresses() RegTxTestBuilder {
	rtb.isCheckRegAndUpdAddr = true
	return rtb
}

func (rtb *regTxTestBuilder) SetMessageForRegCheck(message string, expectedInclusion bool) RegTxTestBuilder {
	rtb.messagesForRegCheck = append(rtb.messagesForRegCheck,
		&utils.MessageForCheck{
			Message:           message,
			ExpectedInclusion: expectedInclusion,
			ActualInclusion:   false,
		})
	return rtb
}

func (rtb *regTxTestBuilder) SetMessageForUpdCheck(message string, expectedInclusion bool) RegTxTestBuilder {
	rtb.messagesForUpdCheck = append(rtb.messagesForUpdCheck,
		&utils.MessageForCheck{
			Message:           message,
			ExpectedInclusion: expectedInclusion,
			ActualInclusion:   false,
		})
	return rtb
}

func (rtb *regTxTestBuilder) SetMessageForDelCheck(message string, expectedInclusion bool) RegTxTestBuilder {
	rtb.messagesForDelCheck = append(rtb.messagesForDelCheck,
		&utils.MessageForCheck{
			Message:           message,
			ExpectedInclusion: expectedInclusion,
			ActualInclusion:   false,
		})
	return rtb
}

func (rtb *regTxTestBuilder) SetDelayBeforeChecking(delayInSec int) RegTxTestBuilder {
	rtb.DelayInSecBeforeChecking = delayInSec
	return rtb
}

func (rtb *regTxTestBuilder) RunTest() {
	if len(rtb.eadAddressesToReg) != 0 || len(rtb.eadWrongAddress) != 0 {
		cmdArgs := utils.ConvertRegistrationAddressesToCmdArgs(rtb.eadAddressesToReg)
		cmdWrongArgs := utils.ConvertRegistrationAddressesStrToCmdArgs(rtb.eadWrongAddress)
		cmdArgs = append(cmdArgs, cmdWrongArgs...)
		cmdArgs = append([]string{utils.CmdArgRegister}, cmdArgs...)
		cmd := exec.Command(rtb.cmdCommandName, cmdArgs...)
		utils.ProcessCommand(rtb.t, cmd, rtb.messagesForRegCheck...)
		for _, messageForCheck := range rtb.messagesForRegCheck {
			assert.Equal(rtb.t, messageForCheck.ExpectedInclusion, messageForCheck.ActualInclusion)
		}
	}

	if len(rtb.eadAddressesToUpd) != 0 {
		cmdArgs := utils.ConvertRegistrationAddressesToCmdArgs(rtb.eadAddressesToUpd)
		cmdArgs = append([]string{utils.CmdArgRegister}, cmdArgs...)
		cmd := exec.Command(rtb.cmdCommandName, cmdArgs...)
		utils.ProcessCommand(rtb.t, cmd, rtb.messagesForUpdCheck...)
		for _, messageForCheck := range rtb.messagesForUpdCheck {
			assert.Equal(rtb.t, messageForCheck.ExpectedInclusion, messageForCheck.ActualInclusion)
		}
	}

	if len(rtb.eadAddressesToDel) != 0 || len(rtb.eadWrongAddressToDel) != 0 {
		cmdArgs := utils.ConvertRegistrationAddressesForDelToCmdArgs(rtb.eadAddressesToDel)
		cmdWrongArgs := utils.ConvertRegistrationAddressesStrToCmdArgs(rtb.eadWrongAddressToDel)
		cmdArgs = append(cmdArgs, cmdWrongArgs...)
		cmdArgs = append([]string{utils.CmdArgDeleteAddress}, cmdArgs...)
		cmd := exec.Command(rtb.cmdCommandName, cmdArgs...)
		utils.ProcessCommand(rtb.t, cmd, rtb.messagesForDelCheck...)
		for _, messageForCheck := range rtb.messagesForDelCheck {
			assert.Equal(rtb.t, messageForCheck.ExpectedInclusion, messageForCheck.ActualInclusion)
		}
	}

	time.Sleep(time.Second * time.Duration(rtb.DelayInSecBeforeChecking))

	if rtb.isCheckRegAndUpdAddr {
		utils.CmpExpectedRegisteredAddressesWithActual(rtb.t, append(rtb.eadAddressesToReg, rtb.eadAddressesToUpd...))
	} else {
		utils.CmpExpectedRegisteredAddressesWithActual(rtb.t, rtb.eadAddressesToCheck)
	}
}
